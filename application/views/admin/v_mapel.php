<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By watur.com">
    <meta name="author" content="watur">

    <title>Welcome To SMK Santo Paulus</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/style.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
            <center><?php echo $this->session->flashdata('msg');?></center>
                <h1 class="page-header">Data
                    <small>Matapelajaran</small>
                    <div class="pull-right"><a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#largeModal"><span class="fa fa-plus"></span> Tambah Matapelajaran</a></div>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>Kode Mata Pelajaran</th>
                        <th>Nama Mata Pelajaran</th>
                        <th style="width:140px;text-align:center;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    foreach ($data->result_array() as $a):
                        $no++;
                        $kdm=$a['kd_mapel'];
                        $nnm=$a['nama_mapel'];
                ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                       <td><?php echo $kdm;?></td>
                        <td><?php echo $nnm;?></td>
                        <td style="text-align:center;">
                            <a class="btn btn-xs btn-warning" href="#modalEdit<?php echo $kdm?>" data-toggle="modal" title="Edit"><span class="fa fa-edit"></span> Edit</a>
                            <a class="btn btn-xs btn-danger" href="#modalHapus<?php echo $kdm?>" data-toggle="modal" title="Hapus"><span class="fa fa-close"></span> Hapus</a>
                        </td
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            </div>
        </div>
        <!-- /.row -->
        <!-- ============ MODAL ADD =============== -->
        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Tambah Matapelajaran</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/mapel/tambah_mapel'?>">
                <div class="modal-body">
                   <div class="form-group">
                        <label class="control-label col-xs-3" >Kode Matapelajaran</label>
                        <div class="col-xs-9">
                            <input name="kd_mapel" class="form-control" type="text" placeholder="Input Kode Matapelajaran..." style="width:280px;" required>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-3" >Nama Pelajaran</label>
                        <div class="col-xs-9">
                            <input name="nama_mapel" class="form-control" type="text" placeholder="Input Nama Matapelajaran..." style="width:280px;" required>
                        </div>
                    </div>
					</div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
             </form>
            </div>
			</div>
        </div>

        <!-- ============ MODAL EDIT =============== -->
        <?php
                    foreach ($data->result_array() as $a) {
                        $kdm=$a['kd_mapel'];
                        $nmm=$a['nama_mapel'];
					
                    ?>
                <div id="modalEdit<?php echo $kdm?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Edit mapel</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/mapel/edit_mapel'?>">
                        <div class="modal-body">
                            <input name="kode" type="hidden" value="<?php echo $kdm;?>">                           
					<div class="form-group">
                        <label class="control-label col-xs-3" >Kode Matapelajaran</label>
                        <div class="col-xs-9">
                            <input name="kd_mapel" class="form-control" type="text" value="<?php echo $kdm;?>" placeholder="Input Kode Matapelajaran..." style="width:280px;" required>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-3" >Nama Matapelajaran</label>
                        <div class="col-xs-9">
                            <input name="nama_mapel" class="form-control" type="text" value="<?php echo $nmm;?>" placeholder="Input Kode Matapelajaran..." style="width:280px;" required>
                        </div>
                    </div>

						</div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!-- ============ MODAL HAPUS =============== -->
        <?php
                    foreach ($data->result_array() as $a) {
                        $kdm=$a['kd_mapel'];
                    ?>
                <div id="modalHapus<?php echo $kdm?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Hapus mapel</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/mapel/hapus_mapel'?>">
                        <div class="modal-body">
                            <p>Yakin mau menghapus data <b><?php echo $kdm;?></b>..?</p>
                                    <input name="kd_mapel" type="hidden" value="<?php echo $kdm; ?>">
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-primary">Hapus</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!--END MODAL-->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo date('Y');?> by watur</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
    
</body>

</html>
