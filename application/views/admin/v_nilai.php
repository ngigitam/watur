<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By watur.com">
    <meta name="author" content="watur">

    <title>Welcome To SMK Santo Paulus</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/style.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
            <center><?php echo $this->session->flashdata('msg');?></center>
                <h1 class="page-header">Data
                    <small>Nilai</small>
                  	<?php
						if($this->session->userdata('akses')=='1'){
					echo '<div class="pull-right"><a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#largeModal"><span class="fa fa-plus"></span> Tambah Nilai</a></div>';
					}else{
					echo "<div class='pull-right'><a href=".base_url()."admin/laporan/lap_data_nilai class='btn btn-sm btn-success' target='_blank'><span class='fa fa-print'></span> Print</a></div>";
					}
					?>
				</h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>NIS</th>
                        <th>Nama</th>
						<th>Kelas</th>
						<th>Tahun Akademik</th>
                        <th>Matematika</th>
                        <th>B. Inggris</th>
                        <th>B. Indonesia</th>
						<th>IPA</th>
                        <th>IPS</th>
                        <?php
							if($this->session->userdata('akses')=='1')
                        echo '<th style="width:140px;text-align:center;">Aksi</th>';
						?>
						
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    foreach ($data->result_array() as $a):
                        $no++;
                        $id=$a['nilai_id'];
                        $nis=$a['siswa_nis'];
                        $nama_siswa=$a['siswa_nama'];
						$kls=$a['siswa_kelas'];
						$thn=$a['siswa_thn_akd'];
                        $mtk=$a['mtk'];
                        $bing=$a['bing'];
                        $bindo=$a['bindo'];
                        $ipa=$a['ipa'];
						$ips=$a['ips'];
                ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                       <td><?php echo $nis;?></td>
                        <td><?php echo $nama_siswa;?></td>
						<td><?php echo $kls;?></td>
						<td><?php echo $thn;?></td>
                        <td><?php echo $mtk;?></td>
                        <td><?php echo $bing;?></td>
                        <td><?php echo $bindo;?></td>
                        <td><?php echo $ipa;?></td>
						 <td><?php echo $ips;?></td>
						 <?php
							if($this->session->userdata('akses')=='1')
                        echo '<td style="text-align:center;">
                            <a class="btn btn-xs btn-warning" href="#modalEdit'.$id.'" data-toggle="modal" title="Edit"><span class="fa fa-edit"></span> Edit</a>
                            <a class="btn btn-xs btn-danger" href="#modalHapus'.$id.'" data-toggle="modal" title="Hapus"><span class="fa fa-close"></span> Hapus</a>
                        </td>';
						?>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            </div>
        </div>
        <!-- /.row -->
        <!-- ============ MODAL ADD =============== -->
        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Tambah Nilai</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/nilai/tambah_nilai1'?>">
                <div class="modal-body">
                    
				 <div class="form-group">
                    <label class="control-label col-xs-3" >NIS</label>
					<div class="col-xs-9">
                    <select class="form-control" name="siswa_id" id="siswa_id" style="width:280px;" required>
                        <option value="">No Selected</option>
                        <?php foreach($siswa as $row):?>
                        <option value="<?php echo $row->siswa_id;?>"><?php echo $row->siswa_nis;?></option>
                        <?php endforeach;?>
                    </select>
					</div>
				 </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nilai Matematika</label>
                        <div class="col-xs-9">
                            <input name="mtk" class="form-control" type="number" min="75" max="100" style="width:70px;" required>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-3" >Nilai B. Inggris</label>
                        <div class="col-xs-9">
                            <input name="bing" class="form-control" type="number" min="75" max="100" style="width:70px;" required>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-3" >Nilai B. Indonesia</label>
                        <div class="col-xs-9">
                            <input name="bindo" class="form-control" type="number" min="75" max="100" style="width:70px;" required>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-3" >IPA</label>
                        <div class="col-xs-9">
                            <input name="ipa" class="form-control" type="number" min="70" max="100" style="width:70px;" required>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label col-xs-3" >IPS</label>
                        <div class="col-xs-9">
                            <input name="ips" class="form-control" type="number" min="70" max="100" style="width:70px;" required>
                        </div>
                    </div>
					</div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
             </form>
            </div>
			</div>
        </div>

        <!-- ============ MODAL EDIT =============== -->
        <?php
                    foreach ($data->result_array() as $a) {
                        $id=$a['nilai_id'];
						$siswa_id=$a['siswa_id'];
                        $nis=$a['siswa_nis'];
                        $nama_siswa=$a['siswa_nama'];
                        $mtk=$a['mtk'];
                        $bing=$a['bing'];
                        $bindo=$a['bindo'];
                        $ipa=$a['ipa'];
						$ips=$a['ips'];
					
                    ?>
                <div id="modalEdit<?php echo $id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Edit Nilai</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/nilai/edit_nilai'?>">
                        <div class="modal-body">
                            <input name="kode" type="hidden" value="<?php echo $id;?>">
					<div class="form-group">
                    <label class="control-label col-xs-3" >NIS</label>
					<div class="col-xs-9">
                    <select class="form-control" name="siswa_id" id="siswa_id" style="width:280px;" required>
                        <?php foreach($siswa as $row){?>
						<option <?php if($row->siswa_id == $siswa_id){ echo 'selected="selected"'; } ?> value="<?php echo $row->siswa_id ?>"><?php echo $row->siswa_nis?> </option>
						<?php }?>
                    </select>
					</div>
				 </div>
                           
					<div class="form-group">
                        <label class="control-label col-xs-3" >Nilai Matematika</label>
                        <div class="col-xs-9">
                            <input name="mtk" class="form-control" type="number" min="75" max="100" style="width:70px;" value="<?php echo $mtk;?>" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nilai B. Inggris</label>
                        <div class="col-xs-9">
                            <input name="bing" class="form-control" type="number" min="75" max="100" style="width:70px;" value="<?php echo $bing;?>" required>
                        </div>
                    </div>
					
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nilai B. Indonesia</label>
                        <div class="col-xs-9">
                            <input name="bindo" class="form-control" type="number" min="75" max="100" style="width:70px;" value="<?php echo $bindo;?>" required>
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label col-xs-3" >IPA</label>
                        <div class="col-xs-9">
                            <input name="ipa" class="form-control" type="number" min="70" max="100" style="width:70px;" value="<?php echo $ipa;?>" required>
                        </div>
                    </div>
				<div class="form-group">
                        <label class="control-label col-xs-3" >IPS</label>
                        <div class="col-xs-9">
                            <input name="ips" class="form-control" type="number" min="70" max="100" style="width:70px;" value="<?php echo $ips;?>"  required>
                        </div>
				</div>

       </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!-- ============ MODAL HAPUS =============== -->
        <?php
                    foreach ($data->result_array() as $a) {
                        $id=$a['nilai_id'];
                    ?>
                <div id="modalHapus<?php echo $id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Hapus Nilai</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/nilai/hapus_nilai'?>">
                        <div class="modal-body">
                            <p>Yakin mau menghapus data <b><?php echo $id;?></b>..?</p>
                                    <input name="kode" type="hidden" value="<?php echo $id; ?>">
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-primary">Hapus</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!--END MODAL-->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo date('Y');?> by watur</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
    
</body>

</html>
