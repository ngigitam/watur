<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By watur.com">
    <meta name="author" content="watur">

    <title>Welcome To SMK Santo Paulus</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/style.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
            <center><?php echo $this->session->flashdata('msg');?></center>
                <h1 class="page-header">Data
                    <small>Laporan</small>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th style="text-align:center;">Laporan</th>
                        <th style="width:200px;text-align:center;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="text-align:center;vertical-align:middle">1</td>
                        <td style="vertical-align:middle;">Laporan Data Siswa</td>
                        <td style="text-align:center;">
                            <a class="btn btn-sm btn-default"href="#" data-toggle="modal" data-target="#largeModalSiswa"><span class="fa fa-eye"></span> Lihat</a>
                            <a class="btn btn-sm btn-default" href="<?php echo base_url().'admin/laporan/lap_data_siswa'?>" target="_blank"><span class="fa fa-print"></span> Print</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;vertical-align:middle">2</td>
                        <td style="vertical-align:middle;">Laporan Data Nilai Siswa</td>
                        <td style="text-align:center;">
                        <a class="btn btn-sm btn-default"href="#" data-toggle="modal" data-target="#largeModalNilai"><span class="fa fa-eye"></span> Lihat</a>
                        <a class="btn btn-sm btn-default" href="<?php echo base_url().'admin/laporan/lap_data_nilai'?>" target="_blank"><span class="fa fa-print"></span> Print</a>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;vertical-align:middle">3</td>
                        <td style="vertical-align:middle;">Laporan Data penjurusan Siswa</td>
                        <td style="text-align:center;">
                        <a class="btn btn-sm btn-default"href="#" data-toggle="modal" data-target="#largeModalPenjurusan"><span class="fa fa-eye"></span> Lihat</a>
                        <a class="btn btn-sm btn-default" href="<?php echo base_url().'admin/laporan/lap_data_penjurusan'?>" target="_blank"><span class="fa fa-print"></span> Print</a>
                        </td>
                    </tr>
					
					<tr>
                        <td style="text-align:center;vertical-align:middle">4</td>
                        <td style="vertical-align:middle;">Laporan Lengkap </td>
                        <td style="text-align:center;">
                        <a class="btn btn-sm btn-default"href="#" data-toggle="modal" data-target="#largeModalSemua"><span class="fa fa-eye"></span> Lihat</a>
                        <a class="btn btn-sm btn-default" href="<?php echo base_url().'admin/laporan/lap_semua_data'?>" target="_blank"><span class="fa fa-print"></span> Print</a>
                        </td>
                    </tr>
     
                </tbody>
            </table>
            </div>
        </div>

        <!-- ============ MODAL Siswa =============== -->
        <div class="modal fade" id="largeModalSiswa" tabindex="-1" role="dialog" aria-labelledby="largeModalSiswa" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Data Siswa</h3>
            </div>
            <div class="modal-body" style="overflow:scroll;">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Kelas</th>
                        <th>Tahun Akademik</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    foreach ($data->result_array() as $a):
                        $no++;
                        $nis=$a['siswa_nis'];
                        $nama_siswa=$a['siswa_nama'];
                        $jk=$a['siswa_jk'];
                        $kls=$a['siswa_kelas'];
                        $thn=$a['siswa_thn_akd'];
                        $status=$a['siswa_status'];
                ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                       <td><?php echo $nis;?></td>
                        <td><?php echo $nama_siswa;?></td>
                        <td><?php echo $jk;?></td>
                        <td><?php echo $kls;?></td>
                        <td><?php echo $thn;?></td>
                        <td><?php echo $status;?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

              </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                </div>
                </form>
            </div>
			</div>
        </div>
        <!--  AND Modal    -->

        <!-- ============ MODAL Nilai =============== -->
        <div class="modal fade" id="largeModalNilai" tabindex="-1" role="dialog" aria-labelledby="largeModalNilai" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Data Nilai Siswa</h3>
            </div>
            <div class="modal-body" style="overflow:scroll;">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>NIS</th>
                        <th>Nama</th>
						<th>Kelas</th>
						<th>Tahun Akademik</th>
                        <th>Matematika</th>
                        <th>B.Inggris</th>
                        <th>B.Indonesia</th>
						<th>IPA</th>
                        <th>IPS</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    foreach ($data->result_array() as $a):
                        $no++;
                        $nis=$a['siswa_nis'];
                        $nama_siswa=$a['siswa_nama'];
						$kls=$a['siswa_kelas'];
						$thn=$a['siswa_thn_akd'];
                        $mtk=$a['mtk'];
                        $bing=$a['bing'];
                        $bindo=$a['bindo'];
                        $ipa=$a['ipa'];
						$ips=$a['ips'];
                ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                        <td><?php echo $nis;?></td>
                         <td><?php echo $nama_siswa;?></td>
						 <td><?php echo $kls;?></td>
						 <td><?php echo $thn;?></td>
                         <td><?php echo $mtk;?></td>
                         <td><?php echo $bing;?></td>
                          <td><?php echo $bindo;?></td>
                          <td><?php echo $ipa;?></td>
                          <td><?php echo $ips;?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

              </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                </div>
                </form>
            </div>
			</div>
        </div>
 <!-- ============ MODAL Penjurusan =============== -->
 <div class="modal fade" id="largeModalPenjurusan" tabindex="-1" role="dialog" aria-labelledby="largeModalPenjurusan" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Data Penjurusan Siswa</h3>
            </div>
            <div class="modal-body" style="overflow:scroll;">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>NIS</th>
			            <th>Nama Siswa</th>
						<th>Kelas</th>
						<th>Tahun Akademik</th>
			            <th>Penjurusan</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    foreach ($data->result_array() as $a):
                        $no++;
                        $nis=$a['siswa_nis'];
                        $nama=$a['siswa_nama'];
						$kls=$a['siswa_kelas'];
						$thn=$a['siswa_thn_akd'];
                        $pnjr=$a['penjurusan'];
                ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                        <td><?php echo $nis;?></td>
                        <td><?php echo $nama; ?></td>
						<td><?php echo $kls; ?></td>
						<td><?php echo $thn;?></td>
                        <td><?php echo $pnjr; ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

              </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                </div>
                </form>
            </div>
			</div>
        </div>
<!-- ============ MODAL Semua =============== -->
 <div class="modal fade" id="largeModalSemua" tabindex="-1" role="dialog" aria-labelledby="largeModalPenjurusan" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Data Penjurusan Siswa</h3>
            </div>
            <div class="modal-body" style="overflow:scroll;">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>NIS</th>
			            <th>Nama Siswa</th>
						<th>Kelas</th>
						<th>Tahun Akademik</th>
						<th>NiLai Metematika</th>
						<th>NiLai B.Inggris</th>
						<th>NiLai B.Indonesia</th>
						<th>IPA</th>
						<th>IPS</th>
			            <th>Penjurusan</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    foreach ($data->result_array() as $a):
                        $no++;
                        $nis=$a['siswa_nis'];
                        $nama=$a['siswa_nama'];
						$kls=$a['siswa_kelas'];
						$thn=$a['siswa_thn_akd'];
						$mtk=$a['mtk'];
                        $bing=$a['bing'];
                        $bindo=$a['bindo'];
						$ipa=$a['ipa'];
						$ips=$a['ips'];
                        $pnjr=$a['penjurusan'];
                ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                        <td><?php echo $nis;?></td>
                        <td><?php echo $nama; ?></td>
						<td><?php echo $kls; ?></td>
						<td><?php echo $thn;?></td>
						<td><?php echo $mtk;?></td>
                        <td><?php echo $bing; ?></td>
						<td><?php echo $bindo; ?></td>
						<td><?php echo $ipa;?></td>
                        <td><?php echo $ips; ?></td>
                        <td><?php echo $pnjr; ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

              </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                </div>
                </form>
            </div>
			</div>
        </div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo date('Y');?> by watur</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
    
</body>

</html>
