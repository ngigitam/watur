<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url().'welcome'?>">Sekolah Damai</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                   <?php $h=$this->session->userdata('akses'); 
						 $u=$this->session->userdata('user'); 
						 $id_user=$this->session->userdata('idadmin');
						 $id_siswa=$this->session->userdata('idsiswa');
						 if($h=='1' || $h=='2' || $h=='4' || $h=='5' || $h=='6'){
							$q=$this->db->query('SELECT * FROM user WHERE user_id="'.$id_user.'"');
							$c=$q->row_array();
							$username=$c['user_nama'];
						}else{
							$q=$this->db->query('SELECT * FROM tbl_siswa WHERE siswa_id="'.$id_siswa.'"');
							$c=$q->row_array();
							$username=$c['siswa_nama'];
						}
					?>
                    <?php if($h=='1'){ ?> 
                     <!--dropdown-->
					 <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="Data"><span class="fa fa-archive" aria-hidden="true"></span> Data</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'admin/pengguna'?>"><span class="fa fa-user" aria-hidden="true"></span> User</a></li>
                            <li><a href="<?php echo base_url().'admin/siswa'?>"><span class="fa fa-users" aria-hidden="true"></span> Siswa</a></li>
							 <!--li><a href="<?php echo base_url().'admin/mapel'?>"><span class="fa fa-book" aria-hidden="true"></span> Matapelajaran</a></li-->
							<li><a href="<?php echo base_url().'admin/nilai'?>"><span class="fa fa-file-o" aria-hidden="true"></span> Nilai</a></li>
                        </ul>
                    </li>

                    <!--ending dropdown-->
					<li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="Data"><span class="fa fa-openid" aria-hidden="true"></span> Penjurusan</a>
                       <ul class="dropdown-menu">
                            <!--li><a href="<?php echo base_url().'admin/penjurusan'?>"><span class="fa fa-medium" aria-hidden="true"></span> Generate Nilai Rata-Rata dan Centroid</a></li-->
                            <li><a href="<?php echo base_url().'admin/penjurusan/iterasi_kmeans'?>"><span class="fa fa-modx" aria-hidden="true"></span> Proses Metode K-Means Clustering</a></li>
                            <li><a href="<?php echo base_url().'admin/penjurusan/iterasi_kmeans_hasil1'?>"><span class="fa fa-medium" aria-hidden="true"></span> Hasil Metode K-Means Clustering</a></li>
						</ul>
					</li>
					
					<li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="Data"><span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> perangkingan</a>
                       <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'admin/penjurusan/hasil_topsis'?>"><span class="glyphicon glyphicon-link" aria-hidden="true"></span> Hasil Perangkingan Metode TOPSIS</a></li>
                            <!--li><a href="<?php echo base_url().'admin/penjurusan/iterasi_kmeans_hasil1'?>"><span class="fa fa-medium" aria-hidden="true"></span> Hasil Metode K-Means Clustering</a></li-->
						</ul>
					</li>
					
                    <li>
                        <a href="<?php echo base_url().'admin/laporan'?>"><span class="fa fa-file"></span> Laporan</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url().'admin/pengaturan'?>"><span class="fa fa-cog"></span> Settings</a>
                    </li>
                    <?php }?>
                    <?php if($h=='2' || $h=='4' || $h=='5' || $h=='6'){ ?> 
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="Data"><span class="fa fa-archive" aria-hidden="true"></span> Data</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'admin/pengguna'?>"><span class="fa fa-user" aria-hidden="true"></span> User</a></li>
                            <li><a href="<?php echo base_url().'admin/siswa'?>"><span class="fa fa-users" aria-hidden="true"></span> Siswa</a></li>
							<!--li><a href="<?php echo base_url().'admin/mapel'?>"><span class="fa fa-book" aria-hidden="true"></span> Matapelajaran</a></li-->
							<li><a href="<?php echo base_url().'admin/nilai'?>"><span class="fa fa-file-o" aria-hidden="true"></span> Nilai</a></li>
							<li><a href="<?php echo base_url().'admin/penjurusan/hasil_penjurusan'?>"><span class="glyphicon glyphicon-random" aria-hidden="true"></span> Hasil Penjurusan Siswa</a></li>
                        </ul>
                    </li>
					<?php if($h=='2'){ ?> 
					<li><a href="<?php echo base_url().'admin/penjurusan/iterasi_kmeans_hasil1'?>"><span class="fa fa-medium" aria-hidden="true"></span> Hasil Metode K-Means Clustering</a></li>
                    <li><a href="<?php echo base_url().'admin/penjurusan/hasil_topsis'?>"><span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> Hasil Perangkingan Metode TOPSIS</a></li>
                    <?php }?>
					
					
					<li>
                        <a href="<?php echo base_url().'admin/laporan'?>"><span class="fa fa-file"></span> Laporan</a>
                    </li>
					
					
                    <?php }?>
                    <?php if($h=='3'){ ?> 
                      <!--dropdown-->
                    <li>
                        <a href="<?php echo base_url().'siswa/siswa'?>"><span class="glyphicon glyphicon-user"></span> Biodata Siswa</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url().'siswa/siswa/penjurusan'?>"><span class="glyphicon glyphicon-random"></span> Hasil Penjurusan</a>
                    </li>
					<!--li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="Data"><span class="fa fa-spinner" aria-hidden="true"></span> Test Rekomendasi</a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url().'siswa/siswa/nilai_uji'?>"><span class="fa fa-file-o" aria-hidden="true"></span> Nilai</a></li>
							<li><a href="<?php echo base_url().'siswa/siswa/iterasi_kmeans'?>"><span class="fa fa-align-center" aria-hidden="true"></span> Hasil Rekomendasi</a></li>
                        </ul>
                    </li-->
					<li><a href="<?php echo base_url().'siswa/siswa/iterasi_kmeans'?>"><span class="fa fa-spinner" aria-hidden="true"></span> Test Rekomendasi</a></li>

                    <?php }?>

                </ul>
				
				<ul class="nav navbar-nav navbar-right">
				<li>
					<a href=""><span class="fa fa-user"></span> <?php echo $username;?></a>
				</li>
				<li>
                        <a href="<?php echo base_url().'administrator/logout'?>"><span class="fa fa-sign-out"></span> Logout</a>
                </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>