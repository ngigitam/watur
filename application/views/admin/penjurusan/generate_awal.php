    <div class="container margin-b70">
      <div class="row">
        <div class="col-md-12">
          <h1>Data Awal</h1>

            <div id="body">
            <a class="btn btn-primary" href="<?php echo base_url(); ?>admin/penjurusan/generate_rata">Proses Data Rata-Rata</a><br><br>
            <div class="table-responsive">
            <table  id="table_data" class="table table-bordered table-striped table-admin">
			<thead>
              <tr>
			  <td>NIS</td>
			  <td>Nama Siswa</td>
			  <td>Matematika</td>
			  <td>B.Inggris</td>
			  <td>B.Indonesia</td>
			   <td>P.AK</td>
			   <td>P.AP</td>
			  </tr>
			  </thead>
              <?php foreach($tbl_nilai->result_array() as $s){ ?>
              <tr>
			  <td><?php echo $s['siswa_nis']; ?></td>
			  <td><?php echo $s['siswa_nama']; ?></td>
			  <td><?php echo $s['mtk']; ?></td>
			  <td><?php echo $s['bing']; ?></td>
			  <td><?php echo $s['bindo']; ?></td>
			  <td><?php echo $s['ipa']; ?></td>
			  <td><?php echo $s['ips']; ?></td>
			  </tr>
              <?php } ?>
            </table>
            </div>
            </div>

            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div>
      </div>
    </div>
