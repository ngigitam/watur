    <div class="container margin-b70">
      <div class="row">
        <div class="col-md-12">
        <?php error_reporting(0); ?>
          <h1>Data Proses Iterasi</h1>


            <div id="body">
            <a class="btn btn-primary" href="<?php echo base_url(); ?>admin/penjurusan/iterasi_kmeans_lanjut">Proses Iterasi Selanjutnya</a><br><br>

            <?php
              $c1a = "";
              $c1b = "";
			  $c1c = "";
              $c1d = "";
			  $c1e = "";

              $c2a = "";
              $c2b = "";
			  $c2c = "";
              $c2d = "";
			  $c2e = "";

              foreach($centroid->result_array() as $c)
              {
				//$no= $c['nomor']; 
                $c1a = $c['c1a'];
                $c1b = $c['c1b'];
				$c1c = $c['c1c'];
                $c1d = $c['c1d'];
				$c1e = $c['c1e'];

                $c2a = $c['c2a'];
                $c2b = $c['c2b'];
				$c2c = $c['c2c'];
                $c2d = $c['c2d'];
				$c2e = $c['c2e'];

              }
              $c1a_b = "";
              $c1b_b = "";
			  $c1c_b = "";
              $c1d_b = "";
			  $c1e_b = "";

              
              $c2a_b = "";
              $c2b_b = "";
			  $c2c_b = "";
              $c2d_b = "";
			  $c2e_b = "";

              
              $hc1=0;
              $hc2=0;

              
              $no=0;
              $arr_c1 = array();
              $arr_c2 = array();
              
              $arr_c1_temp = array();
              $arr_c2_temp = array();
			  $arr_c3_temp = array();
              $arr_c4_temp = array();
			  $arr_c5_temp = array();

              foreach($q->result_array() as $hq)
              {
				 $itrs=$hq['iterasi'];
			  }
            ?>
			<center><h3>Iterasi ke-<?php echo $itrs; ?></h3></center>
            <div class="table-responsive">
            <table  id="table_data" class="table table-bordered table-admin">
              <tr align="center">
			  <td rowspan="2">NIS</td>
			  <td rowspan="2">Nama Siswa</td>
			  <td rowspan="2">Matematika</td>
			  <td rowspan="2">B. Inggris</td>
			  <td rowspan="2">B. Indonesia</td>
			  <td rowspan="2">P. AK</td>
			  <td rowspan="2">P. AP</td>
              <td colspan="5">Centroid 1</td>
			  <td colspan="5">Centroid 2</td>
				<td rowspan="2">C1</td>
				<td rowspan="2">C2</td>

              </tr>
              <tr align="center">
              <td><?php echo $c1a; ?></td>
			  <td><?php echo $c1b; ?></td>
			  <td><?php echo $c1c; ?></td>
			  <td><?php echo $c1d; ?></td>
			  <td><?php echo $c1e; ?></td>
              <td><?php echo $c2a; ?></td>
			  <td><?php echo $c2b; ?></td>
			  <td><?php echo $c2c; ?></td>
			  <td><?php echo $c2d; ?></td>
			  <td><?php echo $c2e; ?></td>
   
              </tr>
              <?php 
              foreach($tbl_nilai->result_array() as $s){ ?>
              <tr>
			  <?php $s['siswa_id']; ?>
			  <td><?php echo $s['siswa_nis']; ?></td>
			  <td><?php echo $s['siswa_nama']; ?></td>
			  <td><?php echo $s['mtk']; ?></td>
			  <td><?php echo $s['bing']; ?></td>
			  <td><?php echo $s['bindo']; ?></td>
			  <td><?php echo $s['ipa']; ?></td>
			  <td><?php echo $s['ips']; ?></td>
				<td colspan="5"><?php  
               $hc1 = sqrt(pow(($s['mtk']-$c1a),2)+pow(($s['bing']-$c1b),2)+pow(($s['bindo']-$c1c),2)+pow(($s['ipa']-$c1d),2)+pow(($s['ips']-$c1e),2));
                echo $hc1;
              ?></td>
              <td colspan="5"><?php 
                $hc2 = sqrt(pow(($s['mtk']-$c2a),2)+pow(($s['bing']-$c2b),2)+pow(($s['bindo']-$c2c),2)+pow(($s['ipa']-$c2d),2)+pow(($s['ips']-$c2e),2));
                echo $hc2;
              ?></td>
              <?php 
              
              if($hc1<=$hc2)
              {
                  $arr_c1[$no] = 1;
              }
              else
              {
                $arr_c1[$no] = '0';
              }
              
              if($hc2<=$hc1)
              {
                  $arr_c2[$no] = 1;
              }
              else
              {
                $arr_c2[$no] = '0';
              }
              
              $arr_c1_temp[$no] = $s['mtk'];
              $arr_c2_temp[$no] = $s['bing'];
              $arr_c3_temp[$no] = $s['bindo'];
			  $arr_c4_temp[$no] = $s['ipa'];
              $arr_c5_temp[$no] = $s['ips'];
              
              $warna1="";
              $warna2="";
              ?>
              <?php if($arr_c1[$no]==1){$warna1='#FFFF00';} else{$warna1='#ccc';} ?><td bgcolor="<?php echo $warna1; ?>"><?php echo $arr_c1[$no] ;?></td>
              <?php if($arr_c2[$no]==1){$warna2='#FFFF00';} else{$warna2='#ccc';} ?><td bgcolor="<?php echo $warna2; ?>"><?php echo $arr_c2[$no] ;?></td>
              </tr>
              <?php
              
              $q = "insert into centroid_temp(siswa_id,iterasi,c1,c2) values('".$s['siswa_id']."','".$id."','".$arr_c1[$no]."','".$arr_c2[$no]."')";
              $this->db->query($q);
              
              $no++; } 
              
           //centroid baru 1.a
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c1_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1a_b = array_sum($arr)/$jum;
              
              //centroid baru 1.b
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c2_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1b_b = array_sum($arr)/$jum;   
				
			 //centroid baru 1.c
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c3_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1c_b = array_sum($arr)/$jum;
			  			 
			//centroid baru 1.d
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c4_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1d_b = array_sum($arr)/$jum; 
			  
			//centroid baru 1.e
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c5_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1e_b = array_sum($arr)/$jum; 
                     
              
              //centroid baru 2.a
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c1_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2a_b = array_sum($arr)/$jum;
              
              //centroid baru 2.b
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c2_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2b_b = array_sum($arr)/$jum;
			  
			   //centroid baru 2.c
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c3_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2c_b = array_sum($arr)/$jum;
			  
			  //centroid baru 2.d
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c4_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2d_b = array_sum($arr)/$jum;
			  
			 //centroid baru 2.e
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c5_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2e_b = array_sum($arr)/$jum;
			  
              
         
              
             $q = "insert into hasil_centroid(c1a,c1b,c1c,c1d,c1e,c2a,c2b,c2c,c2d,c2e) values('".$c1a_b."','".$c1b_b."','".$c1c_b."','".$c1d_b."','".$c1e_b."','".$c2a_b."','".$c2b_b."','".$c2c_b."','".$c2d_b."','".
             $c2e_b."')";
			 
			 // $q = "insert into hasil_centroid(c1a,c1b,c2a,c2b) values('".$c1a_b."','".$c1b_b."','".$c2a_b."','".$c2b_b."')";
              $this->db->query($q);
              
              
              
              ?>
            </table>
            </div>

            </div>

            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div>
      </div>
    </div>
