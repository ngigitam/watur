    <div class="container margin-b70">
      <div class="row">
        <div class="col-md-12">
        <?php error_reporting(0); ?>
          <h1>Data Awal</h1>

            <div id="body">
            <a class="btn btn-primary" href="<?php echo base_url(); ?>admin/penjurusan/iterasi_kmeans_lanjut">Proses Iterasi Selanjutnya</a><br><br>
            <div class="table-responsive">
            <table  id="table_data" class="table table-bordered table-admin">
              <tr align="center">
			  <td rowspan="2">NIS</td>
			  <td rowspan="2">Nama Siswa</td>
			  <td rowspan="2">Nilai Matematika</td>
			  <td rowspan="2">Nilai B.Ing</td>
			  <td rowspan="2">Nilai B.Ind</td>
			  <td rowspan="2">IPA</td>
			  <td rowspan="2">IPS</td>
              <td colspan="5">Centroid 1</td>
			  <td colspan="5">Centroid 2</td>
			<td rowspan="2">C1</td>
			<td rowspan="2">C2</td>
              </tr>
              <tr align="center">
			   <?php
              foreach($c1->result_array() as $hc1)
			  {
			/*	$x = rand(1,5);
				$nilai1[1] = $hc1['mtk'];
				$nilai1[2] = $hc1['bing'];
				$nilai1[3] = $hc1['bindo'];
				$nilai1[4] = $hc1['ipa'];
        $nilai1[5] = $hc1['ips'];
        */
        $nilai11 = $hc1['mtk'];
				$nilai12 = $hc1['bing'];
				$nilai13 = $hc1['bindo'];
				$nilai14 = $hc1['ipa'];
        $nilai15 = $hc1['ips'];
			  }             
			foreach($c2->result_array() as $hc2)
			{
				/*$x = rand(1,5);
				$nilai2[1] = $hc2['mtk'];
				$nilai2[2] = $hc2['bing'];
				$nilai2[3] = $hc2['bindo'];
				$nilai2[4] = $hc2['ipa'];
        $nilai2[5] = $hc2['ips'];
        */
        $nilai21 = $hc2['mtk'];
				$nilai22 = $hc2['bing'];
				$nilai23 = $hc2['bindo'];
				$nilai24 = $hc2['ipa'];
        $nilai25 = $hc2['ips'];
			}
			  ?>
			  <td><?php echo $nilai11?></td>
			  <td><?php echo $nilai12?></td>
			  <td><?php echo $nilai13?></td>
			  <td><?php echo $nilai14?></td>
			  <td><?php echo $nilai15?></td>
			  
			  <td><?php echo $nilai21; ?></td>
			  <td><?php echo $nilai22; ?></td>
			  <td><?php echo $nilai23; ?></td>
			  <td><?php echo $nilai24; ?></td>
			  <td><?php echo $nilai25; ?></td>
			</tr>
              <?php

        $c1a = $nilai11;
        $c1b = $nilai12;
			  $c1c = $nilai13;
			  $c1d = $nilai14;
			  $c1e = $nilai15;

        $c2a = $nilai21;
        $c2b = $nilai22;
			  $c2c = $nilai23;
			  $c2d = $nilai24;
			  $c2e = $nilai25;
			  
              $c1a_b = "";
              $c1b_b = "";
			  $c1c_b = "";
              $c1d_b = "";
			  $c1e_b = "";

              $c2a_b = "";
              $c2b_b = "";
			  $c2c_b = "";
              $c2d_b = "";
			  $c2e_b = "";

              $hc1=0;
              $hc2=0;

              
              $no=0;
              $arr_c1 = array();
              $arr_c2 = array();

              
              $arr_c1_temp = array();
              $arr_c2_temp = array();
			  $arr_c3_temp = array();
              $arr_c4_temp = array();
			  $arr_c5_temp = array();
          
              $this->db->query('truncate table centroid_temp');
              $this->db->query('truncate table hasil_centroid');
			 $q = "insert into hasil_centroid(nomor,c1a,c1b,c1c,c1d,c1e,c2a,c2b,c2c,c2d,c2e) values(0,'".$c1a."','".$c1b."','".$c1c."','".$c1d."','".$c1e."','".$c2a."','".$c2b."','".$c2c."','".$c2d."','".
             $c2e."')";
			 $this->db->query($q);
              foreach($tbl_nilai->result_array() as $s){ ?>
              <tr>
			  <?php $s['siswa_id']; ?>
			  <td><?php echo $s['siswa_nis']; ?></td>
			  <td><?php echo $s['siswa_nama']; ?></td>
			  <td><?php echo $s['mtk']; ?></td>
			  <td><?php echo $s['bing']; ?></td>
			  <td><?php echo $s['bindo']; ?></td>
			  <td><?php echo $s['ipa']; ?></td>
			  <td><?php echo $s['ips']; ?></td>
				<td colspan="5" align="center"><?php 
                $hc1 = sqrt(pow(($s['mtk']-$c1a),2)+pow(($s['bing']-$c1b),2)+pow(($s['bindo']-$c1c),2)+pow(($s['ipa']-$c1d),2)+pow(($s['ips']-$c1e),2));
                echo $hc1=number_format($hc1,4);
              ?></td>
              <td colspan="5" align="center"><?php 
                $hc2 = sqrt(pow(($s['mtk']-$c2a),2)+pow(($s['bing']-$c2b),2)+pow(($s['bindo']-$c2c),2)+pow(($s['ipa']-$c2d),2)+pow(($s['ips']-$c2e),2));
                echo $hc2=number_format($hc2,4);
              ?></td>
              <?php 
              
              if($hc1<=$hc2)
              {
                  $arr_c1[$no] = 1;
              }
              else
              {
                $arr_c1[$no] = '0';
              }
              
              if($hc2<=$hc1)
              {
                  $arr_c2[$no] = 1;
              }
              else
              {
                $arr_c2[$no] = '0';
              }
              
              $arr_c1_temp[$no] = $s['mtk'];
              $arr_c2_temp[$no] = $s['bing'];
              $arr_c3_temp[$no] = $s['bindo'];
			  $arr_c4_temp[$no] = $s['ipa'];
              $arr_c5_temp[$no] = $s['ips'];
              
              $warna1="";
              $warna2="";
              ?>
              <?php if($arr_c1[$no]==1){$warna1='#FFFF00';} else{$warna1='#ccc';} ?><td bgcolor="<?php echo $warna1; ?>"><?php echo $arr_c1[$no] ;?></td>
              <?php if($arr_c2[$no]==1){$warna2='#FFFF00';} else{$warna2='#ccc';} ?><td bgcolor="<?php echo $warna2; ?>"><?php echo $arr_c2[$no] ;?></td>
              </tr>
              <?php
              
              $q = "insert into centroid_temp(siswa_id,iterasi,c1,c2) values('".$s['siswa_id']."',0,0,0)";
			  $this->db->query($q);
			  
			  $q = "insert into centroid_temp(siswa_id,iterasi,c1,c2) values('".$s['siswa_id']."',1,'".$arr_c1[$no]."','".$arr_c2[$no]."')";
              $this->db->query($q);
              
              $no++; } 
              
              //centroid baru 1.a
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c1_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1a_b = array_sum($arr)/$jum;
              
              //centroid baru 1.b
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c2_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1b_b = array_sum($arr)/$jum;   
				
			 //centroid baru 1.c
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c3_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1c_b = array_sum($arr)/$jum;
			  			 
			//centroid baru 1.d
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c4_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1d_b = array_sum($arr)/$jum; 
			  
			//centroid baru 1.e
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c1);$i++)
              {
                $arr[$i] = $arr_c5_temp[$i]*$arr_c1[$i];
                if($arr_c1[$i]==1)
                {
                  $jum++;
                }
              }
              $c1e_b = array_sum($arr)/$jum; 
                     
              
              //centroid baru 2.a
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c1_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2a_b = array_sum($arr)/$jum;
              
              //centroid baru 2.b
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c2_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2b_b = array_sum($arr)/$jum;
			  
			   //centroid baru 2.c
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c3_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2c_b = array_sum($arr)/$jum;
			  
			  //centroid baru 2.d
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c4_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2d_b = array_sum($arr)/$jum;
			  
			 //centroid baru 2.e
              $jum = 0;
              $arr = array();
              for($i=0;$i<count($arr_c2);$i++)
              {
                $arr[$i] = $arr_c5_temp[$i]*$arr_c2[$i];
                if($arr_c2[$i]==1)
                {
                  $jum++;
                }
              }
              $c2e_b = array_sum($arr)/$jum;
			  
              
         
              
             $q = "insert into hasil_centroid(nomor,c1a,c1b,c1c,c1d,c1e,c2a,c2b,c2c,c2d,c2e) values(1,'".$c1a_b."','".$c1b_b."','".$c1c_b."','".$c1d_b."','".$c1e_b."','".$c2a_b."','".$c2b_b."','".$c2c_b."','".$c2d_b."','".
             $c2e_b."')";
			 $this->db->query($q);
              
              
              
              ?>
            </table>
            </div>
            </div>

            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div>
      </div>
    </div>
