<?php
  foreach ($v_data->result() as $hq): ?>
<h3 align="center">Iterasi ke-<?php echo $hq->iterasi; ?></h3>
<div class="table-responsive">
            <table  id="table_data" class="table table-bordered table-admin">
              <tr align="center">
			  <td rowspan="2">NIS</td>
			  <td rowspan="2">Nama Siswa</td>
			  <td rowspan="2">Nilai Matematika</td>
			  <td rowspan="2">Nilai B.Ing</td>
			  <td rowspan="2">Nilai B.Ind</td>
			  <td rowspan="2">IPA</td>
			  <td rowspan="2">IPS</td>
              <td colspan="5">Centroid 1</td>
			  <td colspan="5">Centroid 2</td>
				<td rowspan="2">C1</td>
				<td rowspan="2">C2</td>
			  </tr>
			  <?php
                $q2 = $this->db->query('select * from centroid_temp LEFT JOIN tbl_siswa ON centroid_temp.siswa_id=tbl_siswa.siswa_id LEFT JOIN tbl_nilai ON tbl_siswa.siswa_id=tbl_nilai.siswa_id LEFT JOIN hasil_centroid ON centroid_temp.iterasi=hasil_centroid.nomor  where iterasi='.$hq->iterasi.'');
                foreach($q2->result() as $tq)
                {
	
				$c1a=$tq->c1a;
				$c1b=$tq->c1b;
				$c1c=$tq->c1c;
				$c1d=$tq->c1d;
				$c1e=$tq->c1e;
				$c2a=$tq->c2a;
				$c2b=$tq->c2b;
				$c2c=$tq->c2c;
				$c2d=$tq->c2d;
				$c2e=$tq->c2e;
                }
              ?>
			  <tr align="center">
              <td><?php echo $c1a=number_format($c1a,4); ?></td>
			  <td><?php echo $c1b=number_format($c1b,4); ?></td>
			  <td><?php echo $c1c=number_format($c1c,4); ?></td>
			  <td><?php echo $c1d=number_format($c1d,4); ?></td>
			  <td><?php echo $c1e=number_format($c1e,4); ?></td>
              <td><?php echo $c2a=number_format($c2a,4); ?></td>
			  <td><?php echo $c2b=number_format($c2b,4); ?></td>
			  <td><?php echo $c2c=number_format($c2c,4); ?></td>
			  <td><?php echo $c2d=number_format($c2d,4); ?></td>
			  <td><?php echo $c2e=number_format($c2e,4); ?></td>
              </tr>
              <?php
			  $hc1=0;
              $hc2=0;
              $no=0;
              $arr_c1 = array();
              $arr_c2 = array();
			  
			  foreach($q2->result() as $tq)
                {
				$nis=$tq->siswa_nis;
				$nama=$tq->siswa_nama;
				$mtk=$tq->mtk;
				$bing=$tq->bing;
				$bindo=$tq->bindo;
				$ipa=$tq->ipa;
				$ips=$tq->ips;
                $warna1="";
                $warna2="";
                if($tq->c1==1){$warna1='#FFFF00';} else{$warna1='#EAEAEA';}
                if($tq->c2==1){$warna2='#FFFF00';} else{$warna2='#EAEAEA';}

              ?>

              <tr align="center">
			  <td><?php echo $nis;?></td>
			  <td><?php echo $nama;?></td>
			  <td><?php echo $mtk;?></td>
			  <td><?php echo $bing;?></td>
			  <td><?php echo $bindo;?></td>
			  <td><?php echo $ipa;?></td>
			  <td><?php echo $ips;?></td>
			  <td colspan="5" align="center"><?php  
               $hc1 = sqrt(pow(($mtk-$c1a),2)+pow(($bing-$c1b),2)+pow(($bindo-$c1c),2)+pow(($ipa-$c1d),2)+pow(($ips-$c1e),2));
                echo $hc1=number_format($hc1,4);
              ?></td>
              <td colspan="5" align="center"><?php 
                $hc2 = sqrt(pow(($mtk-$c2a),2)+pow(($bing-$c2b),2)+pow(($bindo-$c2c),2)+pow(($ipa-$c2d),2)+pow(($ips-$c2e),2));
                echo $hc2=number_format($hc2,4);
              ?></td>
			  <?php
			  if($hc1<=$hc2)
              {
                  $arr_c1[$no] = 1;
              }
              else
              {
                $arr_c1[$no] = '0';
              }
              
              if($hc2<=$hc1)
              {
                  $arr_c2[$no] = 1;
              }
              else
              {
                $arr_c2[$no] = '0';
              }
			  ?>
			  <?php if($arr_c1[$no]==1){$warna1='#FFFF00';} else{$warna1='#ccc';} ?><td bgcolor="<?php echo $warna1; ?>"><?php echo $arr_c1[$no] ;?></td>
              <?php if($arr_c2[$no]==1){$warna2='#FFFF00';} else{$warna2='#ccc';} ?><td bgcolor="<?php echo $warna2; ?>"><?php echo $arr_c2[$no] ;?></td>
              
			  </tr>
              <?php
                }
              ?>
</table>
<?php
  endforeach;
?>
 </div>