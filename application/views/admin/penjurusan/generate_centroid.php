    <div class="container margin-b70">
      <div class="row">
        <div class="col-md-12">
          <h1>Data Akhir</h1>

          <div id="body">
          <a  class="btn btn-primary" href="<?php echo base_url(); ?>admin/penjurusan/generate_rata">Proses Data Rata-Rata</a> <a  class="btn btn-success" href="<?php echo base_url(); ?>admin/penjurusan/generate_centroid">Proses Data Akhir</a><br><br>
          <div class="table-responsive">
            <table  id="table_data" class="table table-bordered table-striped table-admin">
            <tr><td>Centroid 1</td><td>IPA</td><td><?php echo $c1; ?></td></tr>
            <tr><td>Centroid 2</td><td>IPS</td><td><?php echo $c2; ?></td></tr>
          </table>
          </div>
          <br>
          <br>
          <div class="table-responsive">
            <table  id="table_data" class="table table-bordered table-striped table-admin">
            <tr align="center">
			  <td>NIS</td>
			  <td>Nama Siswa</td>
			  <td>Matematika</td>
			  <td>B.Inggris</td>
			  <td>B.Indonesia</td>
			   <td>P.AK</td>
			   <td>P.AP</td>
			<td>Rata-Rata</td>
			<td colspan="2">Distance</td>
			<td>Jurusan</td></tr>
            <?php foreach($tbl_nilai->result_array() as $s){ ?>
            <tr>
			  <td><?php echo $s['siswa_nis']; ?></td>
			  <td><?php echo $s['siswa_nama']; ?></td>
			  <td><?php echo $s['mtk']; ?></td>
			  <td><?php echo $s['bing']; ?></td>
			  <td><?php echo $s['bindo']; ?></td>
			  <td><?php echo $s['ipa']; ?></td>
			  <td><?php echo $s['ips']; ?></td>
			<td><?php echo $s['rata_rata']; ?></td>
			<td><?php echo $s['d1']; ?></td>
			<td><?php echo $s['d2']; ?></td>
			<!--td><?php echo $s['d3']; ?></td>
			<td><?php echo $s['d4']; ?></td>
			<td><?php echo $s['d5']; ?></td-->
			<td><?php echo $s['predikat']; ?></td>
			</tr>
            <?php } ?>
          </table>
          </div>
          </div>

          <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div>
      </div>
    </div>
