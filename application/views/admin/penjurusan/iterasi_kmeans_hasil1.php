    <div class="container margin-b70">
      <div class="row">
        <div class="col-md-12">
        <?php error_reporting(0); ?>
          <h1>Data Hasil Penjurusan</h1>
		  	<button class="tampil btn btn-sm btn-success">Tampilkan Data Awal </button>
			<button class="sembunyi btn btn-sm btn-danger">Sembunyikan Data Awal</button>
			<button class="tampil1 btn btn-sm btn-success">Tampilkan Data Iterasi </button>
			<button class="sembunyi1 btn btn-sm btn-danger">Sembunyikan Data Iterasi</button>
			<p>
			<div class="form-group">
                    <label class="control-label col-xs-1">Iterasi</label>
                    <select class="form-control" name="iterasi"  style="width:138px;height:30px;" onchange="cek_data()">                 
                        <?php foreach($q1 as $row):?>
                        <option value="<?php echo $row->iterasi;?>" ><?php echo $row->iterasi;?></option>
                        <?php endforeach;?>
                    </select>
				 </div>
            <div id="body">
		 <div class="loading"></div>
		<div class="tampilkan_data"></div>
		<div class="data_awal">
		<h3 align="center">Data Awal</h3>
			<div class="table-responsive">
            <table  id="table_data" class="table table-bordered table-admin">
              <tr align="center">
			  <td rowspan="2">NIS</td>
			  <td rowspan="2">Nama Siswa</td>
			  <td rowspan="2">Nilai Matematika</td>
			  <td rowspan="2">Nilai B.Ing</td>
			  <td rowspan="2">Nilai B.Ind</td>
			  <td rowspan="2">IPA</td>
			  <td rowspan="2">IPS</td>
              <td colspan="5">Centroid 1</td>
			  <td colspan="5">Centroid 2</td>
			<td rowspan="2">C1</td>
			<td rowspan="2">C2</td>
              </tr>
              <tr align="center">
			   <?php
              foreach($c->result_array() as $hc)
			  {
				$c1a = $hc['c1a'];
				$c1b = $hc['c1b'];
				$c1c = $hc['c1c'];
				$c1d = $hc['c1d'];
				$c1e = $hc['c1e'];
				$c2a = $hc['c2a'];
				$c2b = $hc['c2b'];
				$c2c = $hc['c2c'];
				$c2d = $hc['c2d'];
				$c2e = $hc['c2e'];
			  }             
			  ?>
			  <td><?php echo $c1a; ?></td>
			  <td><?php echo $c1b; ?></td>
			  <td><?php echo $c1c; ?></td>
			  <td><?php echo $c1d; ?></td>
			  <td><?php echo $c1e; ?></td>
			  
			  <td><?php echo $c2a; ?></td>
			  <td><?php echo $c2b; ?></td>
			  <td><?php echo $c2c; ?></td>
			  <td><?php echo $c2d; ?></td>
			  <td><?php echo $c2e; ?></td>
			</tr>
              <?php

              $c1a = $c1a;
              $c1b = $c1b;
			  $c1c = $c1c;
			  $c1d = $c1d;
			  $c1e = $c1e;

              $c2a = $c2a;
              $c2b = $c2b;
			  $c2c = $c2c;
			  $c2d = $c2d;
			  $c2e = $c2e;

              $c1a_b = "";
              $c1b_b = "";
			  $c1c_b = "";
              $c1d_b = "";
			  $c1e_b = "";

              $c2a_b = "";
              $c2b_b = "";
			  $c2c_b = "";
              $c2d_b = "";
			  $c2e_b = "";

              $hc1=0;
              $hc2=0;

              $no=0;
              $arr_c1 = array();
              $arr_c2 = array();
             
              $arr_c1_temp = array();
              $arr_c2_temp = array();
			  $arr_c3_temp = array();
              $arr_c4_temp = array();
			  $arr_c5_temp = array();
          
              foreach($data_hasil->result_array() as $s){ ?>
              <tr>
			  <?php $s['siswa_id']; ?>
			  <td><?php echo $s['siswa_nis']; ?></td>
			  <td><?php echo $s['siswa_nama']; ?></td>
			  <td><?php echo $s['mtk']; ?></td>
			  <td><?php echo $s['bing']; ?></td>
			  <td><?php echo $s['bindo']; ?></td>
			  <td><?php echo $s['ipa']; ?></td>
			  <td><?php echo $s['ips']; ?></td>
				<td colspan="5" align="center"><?php 
                $hc1 = sqrt(pow(($s['mtk']-$c1a),2)+pow(($s['bing']-$c1b),2)+pow(($s['bindo']-$c1c),2)+pow(($s['ipa']-$c1d),2)+pow(($s['ips']-$c1e),2));
                echo $hc1=number_format($hc1,4);
              ?></td>
              <td colspan="5" align="center"><?php 
                $hc2 = sqrt(pow(($s['mtk']-$c2a),2)+pow(($s['bing']-$c2b),2)+pow(($s['bindo']-$c2c),2)+pow(($s['ipa']-$c2d),2)+pow(($s['ips']-$c2e),2));
                echo $hc2=number_format($hc2,4);
              ?></td>
              <?php 
              
              if($hc1<=$hc2)
              {
                  $arr_c1[$no] = 1;
              }
              else
              {
                $arr_c1[$no] = '0';
              }
              
              if($hc2<=$hc1)
              {
                  $arr_c2[$no] = 1;
              }
              else
              {
                $arr_c2[$no] = '0';
              }
              
              $arr_c1_temp[$no] = $s['mtk'];
              $arr_c2_temp[$no] = $s['bing'];
              $arr_c3_temp[$no] = $s['bindo'];
			  $arr_c4_temp[$no] = $s['ipa'];
              $arr_c5_temp[$no] = $s['ips'];
              
              $warna1="";
              $warna2="";
              ?>
              <?php if($arr_c1[$no]==1){$warna1='#FFFF00';} else{$warna1='#ccc';} ?><td bgcolor="<?php echo $warna1; ?>"><?php echo $arr_c1[$no] ;?></td>
              <?php if($arr_c2[$no]==1){$warna2='#FFFF00';} else{$warna2='#ccc';} ?><td bgcolor="<?php echo $warna2; ?>"><?php echo $arr_c2[$no] ;?></td>
              </tr>
			  <?php
			  }
				?>

            </table>
            </div>
		
		</div>
		<br>
             <br>
            <table  id="table_data" class="table table-bordered table-admin">
              <tr align="center">
			        <td>NIS</td>
			        <td>Nama Siswa</td>
			        <td>Penjurusan</td></tr>
              <?php
                foreach($data_hasil->result_array() as $h)
                {
					$nis=$h['siswa_nis'];
					$nama=$h['siswa_nama'];
					$pnjr=$h['penjurusan'];
              ?>
              <tr align="center">
			  <td><?php echo $nis;?></td>
              <td><?php echo $nama; ?></td>
              <td><?php echo $pnjr; ?></td>
              </tr>
              <?php
                }
              ?>
            </table>
            </div>
			</div>

            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div>
      </div>
    </div>
