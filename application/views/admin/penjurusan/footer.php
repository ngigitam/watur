  <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo date('Y');?> by watur</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

	

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
	<script type="text/javascript">
      function cek_data()
      {
        sel_iterasi = $('[name="iterasi"]');
        $.ajax({
          type : "POST",
          data: "cari="+1+"&iterasi="+sel_iterasi.val(),
		 
          url  : "<?php echo base_url().'admin/penjurusan/view_iterasi';?>",
          cache: false,
          beforeSend: function() {
            sel_iterasi.attr('disabled', true);
            $('.loading').html('Loading...');
          },
          success: function(data){
            sel_iterasi.attr('disabled', false);
            $('.loading').html('');
            $('.tampilkan_data').html(data);
          }
        });
       return false;
      }
    </script>
	
	<script type="text/javascript">
	 $(document).ready(function(){
        $('.data_awal').show();        
        $('.tampil').click(function(){
			$('.data_awal').show();
        });
        $('.sembunyi').click(function(){
			$('.data_awal').hide();        
        });
		$('.tampil1').click(function(){
			$('.tampilkan_data').show();
        });
		$('.sembunyi1').click(function(){
			$('.tampilkan_data').hide();        
        });
	});

    </script>

    
</body>

</html>
