<html lang="en" moznomarginboxes mozdisallowselectionprint>
<head>
    <title>Laporan Data Penjurusan</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/laporan.css')?>"/>
</head>
<body onload="window.print()">
<div id="laporan">
<table align="center" style="width:900px; border-bottom:3px double;border-top:none;border-right:none;border-left:none;margin-top:5px;margin-bottom:20px;">
<tr>
    <td><img width="880px" height="113px" src="<?php echo base_url().'assets/img/kop1.png'?>"/></td>
</tr>
</table>

<table border="0" align="center" style="width:800px; border:none;margin-top:5px;margin-bottom:0px;">
<tr>
    <td colspan="2" style="width:800px;paddin-left:20px;"><center><h4>LAPORAN DATA PENJURUSAN SISWA</h4></center><br/></td>
</tr>
                       
</table>
 
<table border="0" align="center" style="width:900px;border:none;">
        <tr>
            <th style="text-align:left"></th>
        </tr>
</table>

<table border="1" align="center" style="width:900px;margin-bottom:20px;">
<thead>
    <tr>
        <th style="width:50px;">No</th>
        <th>NIS</th>
        <th>Nama</th>
		<th>kelas</th>
		<th>Tahun Akademik</th>
        <th>Penjurusan</th>
    </tr>
</thead>
<tbody>
<?php 
$no=0;
    foreach ($data->result_array() as $i) {
        $no++;
        $nis=$i['siswa_nis'];
        $nama=$i['siswa_nama'];
		$kls=$i['siswa_kelas'];
		$thn=$i['siswa_thn_akd'];
        $pnjr=$i['penjurusan'];
?>
    <tr>
        <td style="text-align:center;"><?php echo $no;?></td>
        <td style="padding-left:5px;"><?php echo $nis;?></td>
        <td style="text-align:center;"><?php echo $nama;?></td>
		<td style="text-align:center;"><?php echo $kls;?></td>
		<td style="text-align:center;"><?php echo $thn;?></td>
        <td style="text-align:center;"><?php echo $pnjr;?></td>
        
    </tr>
<?php }?>
</tbody>
<tfoot>

</tfoot>
</table>

</div>
</body>
</html>_