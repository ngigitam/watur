<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By watur.com">
    <meta name="author" content="watur">

    <title>Welcome To SMK Santo Pauluss</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/style.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
            <center><?php echo $this->session->flashdata('msg');?></center>
                <h1 class="page-header">Data
                    <small>Siswa</small>
		<?php
		if($this->session->userdata('akses')=='1'){
		echo '<div class="pull-right"><a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#largeModal"><span class="fa fa-plus"></span> Tambah Siswa</a></div>';
		}else{
        echo "<div class='pull-right'><a href=".base_url()."admin/laporan/lap_data_siswa class='btn btn-sm btn-success' target='_blank'><span class='fa fa-print'></span> Print</a></div>";
		}
		?>  
			</h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">No</th>
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>Jenis Kelamin</th>
                        <th>Kelas</th>
                        <th>Tahun Akadmik</th>
                        <th>Status</th>
						<?php
							if($this->session->userdata('akses')=='1')
                        echo '<th style="width:140px;text-align:center;">Aksi</th>';
						?>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    foreach ($data->result_array() as $a):
                        $no++;
                        $id=$a['siswa_id'];
                        $nis=$a['siswa_nis'];
                        $nama_siswa=$a['siswa_nama'];
                        $jk=$a['siswa_jk'];
                        $kls=$a['siswa_kelas'];
                        $thn=$a['siswa_thn_akd'];
                        $status=$a['siswa_status'];
                ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                       <td><?php echo $nis;?></td>
                        <td><?php echo $nama_siswa;?></td>
                        <td><?php echo $jk;?></td>
                        <td><?php echo $kls;?></td>
                        <td><?php echo $thn;?></td>
                        <td><?php echo $status;?></td>
						<?php
							if($this->session->userdata('akses')=='1')
                        echo '<td style="text-align:center;">
                            <a class="btn btn-xs btn-warning" href="#modalEditsiswa'.$id.'" data-toggle="modal" title="Edit"><span class="fa fa-edit"></span> Edit</a>
                            <a class="btn btn-xs btn-danger" href="#modalHapussiswa'.$id.'" data-toggle="modal" title="Hapus"><span class="fa fa-close"></span> Hapus</a>
							</td>';
						?>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            </div>
        </div>
        <!-- /.row -->
        <!-- ============ MODAL ADD =============== -->
        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Tambah siswa</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/siswa/tambah_siswa1'?>">
                <div class="modal-body">
                    
                     <div class="form-group">
                        <label class="control-label col-xs-3" >NIS</label>
                        <div class="col-xs-9">
                            <input name="nis" class="form-control" type="text" placeholder="Input NIS..." style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama</label>
                        <div class="col-xs-9">
                            <input name="nama" class="form-control" type="text" placeholder="Input Nama..." style="width:280px;" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Jenis Kelamin</label>
                        <div class="col-xs-9">
                            <select name="jk" class="form-control" style="width:280px;" required>
                                <option value="1">Laki-Laki</option>
                                <option value="2">Perempuan</option>
                            </select>
                        </div>
                    </div> 
                 <div class="form-group">
                        <label class="control-label col-xs-3" >Kelas</label>
                        <div class="col-xs-9">
                            <input name="kls" class="form-control" type="text" placeholder="Input Kelas..." style="width:280px;" required>
                        </div>
                    </div>
                  <div class="form-group">
                        <label class="control-label col-xs-3" >Tahun Akademik</label>
                        <div class="col-xs-9">
                            <input name="thn" class="form-control" type="text" placeholder="Input Tahun Akademik..." style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Password</label>
                        <div class="col-xs-9">
                            <input name="password" class="form-control" type="password" placeholder="Input Password..." style="width:280px;" required>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Ulangi Password</label>
                        <div class="col-xs-9">
                            <input name="password2" class="form-control" type="password" placeholder="Ulangi Password..." style="width:280px;" required>
                        </div>
                    </div> 

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
            </form>
            </div>
            </div>
        </div>

        <!-- ============ MODAL EDIT =============== -->
        <?php
                    foreach ($data->result_array() as $a) {
                        $id=$a['siswa_id'];
                        $nis=$a['siswa_nis'];
                        $nama_siswa=$a['siswa_nama'];
                        $jk=$a['siswa_jk'];
                        $kls=$a['siswa_kelas'];
                        $thn=$a['siswa_thn_akd'];
                        $status=$a['siswa_status'];
                    ?>
                <div id="modalEditsiswa<?php echo $id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Edit siswa</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/siswa/edit_siswa'?>">
                        <div class="modal-body">
                            <input name="kode" type="hidden" value="<?php echo $id;?>">
                            
                 <div class="form-group">
                        <label class="control-label col-xs-3" >NIS</label>
                        <div class="col-xs-9">
                            <input name="nis" class="form-control" type="text" value="<?php echo $nis;?>" placeholder="Input NIS..." style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama</label>
                        <div class="col-xs-9">
                            <input name="nama" class="form-control" type="text" value="<?php echo $nama_siswa;?>" placeholder="Input Nama..." style="width:280px;" required>
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label col-xs-3" >Jenis Kelamin</label>
                        <div class="col-xs-9">
                            <select name="jk" class="form-control" style="width:280px;" required>
                            <option value="1" <?php if(!empty($jk) && $jk == "Laki-Laki") echo 'selected'; ?>>Laki-Laki</option>
                            <option value="2" <?php if(!empty($jk) && $jk == "Perempuan") echo 'selected'; ?>>Perempuan</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Kelas</label>
                        <div class="col-xs-9">
                            <input name="kls" class="form-control" type="text" value="<?php echo $kls;?>" placeholder="Input Kelas..." style="width:280px;" required>
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label col-xs-3" >Tahun Akademik</label>
                        <div class="col-xs-9">
                            <input name="thn" class="form-control" type="text" value="<?php echo $thn;?>" placeholder="Input Tahun Akademik..." style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Password</label>
                        <div class="col-xs-9">
                            <input name="password" class="form-control" type="password" placeholder="Input Password..." style="width:280px;">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Ulangi Password</label>
                        <div class="col-xs-9">
                            <input name="password2" class="form-control" type="password" placeholder="Ulangi Password..." style="width:280px;">
                        </div>
                    </div> 
                    
                   <div class="form-group">
                        <label class="control-label col-xs-3" >Status</label>
                        <div class="col-xs-9">
                            <select name="status" class="form-control" style="width:280px;" required>
                            <option value="0" <?php if(!empty($status) && $status == "Tidak Aktif") echo 'selected'; ?>>Tidak Aktif</option>
                            <option value="1" <?php if(!empty($status) && $status == "Aktif") echo 'selected'; ?>>Aktif</option>
                            </select>
                        </div>
                    </div>

                </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!-- ============ MODAL HAPUS =============== -->
        <?php
                    foreach ($data->result_array() as $a) {
                        $id=$a['siswa_id'];
                        $nis=$a['siswa_nis'];
                        $nama_siswa=$a['siswa_nama'];
                        $jk=$a['siswa_jk'];
                        $kls=$a['siswa_kelas'];
                        $thn=$a['siswa_thn_akd'];
                        $status=$a['siswa_status'];
                    ?>
                <div id="modalHapussiswa<?php echo $id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Hapus Siswa</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/siswa/hapus_siswa'?>">
                        <div class="modal-body">
                            <p>Yakin mau menghapus data <b><?php echo $nis;?></b>..?</p>
                                    <input name="kode" type="hidden" value="<?php echo $id; ?>">
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-primary">Hapus</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!--END MODAL-->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo date('Y');?> by watur</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
    
</body>

</html>
