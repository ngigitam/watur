    <div class="container margin-b70">
      <div class="row">
        <div class="col-md-12">
        <?php error_reporting(0); ?>
          <h1>Data Hasil Iterasi</h1>
            <div id="body">
            <a class="btn btn-primary" href="<?php echo base_url(); ?>admin/penjurusan/iterasi_kmeans">Mulai Awal</a><br><br>
            <?php
              foreach($q->result_array() as $hq)
              {
            ?>
            <center><h3>Iterasi ke-<?php echo $hq['iterasi']; ?></h3></center>
            <div class="table-responsive">
            <table  id="table_data" class="table table-bordered table-admin">
              <tr align="center">
			  <td rowspan="2">NIS</td>
			  <td rowspan="2">Nama Siswa</td>
			  <td rowspan="2">Matematika</td>
			  <td rowspan="2">B. Inggris</td>
			  <td rowspan="2">B. Indonesia</td>
			  <td rowspan="2">P. AK</td>
			  <td rowspan="2">P. AP</td>
              <td colspan="5">Centroid 1</td>
			  <td colspan="5">Centroid 2</td>
				<td rowspan="2">C1</td>
				<td rowspan="2">C2</td>
			  </tr>
			  <?php
                $q2 = $this->db->query('select * from centroid_temp1 LEFT JOIN tbl_siswa ON centroid_temp1.siswa_id=tbl_siswa.siswa_id LEFT JOIN tbl_nilai1 ON tbl_siswa.siswa_id=tbl_nilai1.siswa_id LEFT JOIN hasil_centroid1 ON centroid_temp1.iterasi=hasil_centroid1.no  where iterasi='.$hq['iterasi'].'');
                foreach($q2->result() as $tq)
                {
	
				$c1a=$tq->c1a;
				$c1b=$tq->c1b;
				$c1c=$tq->c1c;
				$c1d=$tq->c1d;
				$c1e=$tq->c1e;
				$c2a=$tq->c2a;
				$c2b=$tq->c2b;
				$c2c=$tq->c2c;
				$c2d=$tq->c2d;
				$c2e=$tq->c2e;
                }
              ?>
			  <tr align="center">
              <td><?php echo $c1a; ?></td>
			  <td><?php echo $c1b; ?></td>
			  <td><?php echo $c1c; ?></td>
			  <td><?php echo $c1d; ?></td>
			  <td><?php echo $c1e; ?></td>
              <td><?php echo $c2a; ?></td>
			  <td><?php echo $c2b; ?></td>
			  <td><?php echo $c2c; ?></td>
			  <td><?php echo $c2d; ?></td>
			  <td><?php echo $c2e; ?></td>
              </tr>
              <?php
			  $hc1=0;
              $hc2=0;

              
              $no=0;
              $arr_c1 = array();
              $arr_c2 = array();
			  
			  foreach($q2->result() as $tq)
                {
				$nis=$tq->siswa_nis;
				$nama=$tq->siswa_nama;
				$mtk=$tq->mtk;
				$bing=$tq->bing;
				$bindo=$tq->bindo;
				$ipa=$tq->ipa;
				$ips=$tq->ips;
                $warna1="";
                $warna2="";
                if($tq->c1==1){$warna1='#FFFF00';} else{$warna1='#EAEAEA';}
                if($tq->c2==1){$warna2='#FFFF00';} else{$warna2='#EAEAEA';}

              ?>

              <tr align="center">
			  <td><?php echo $nis;?></td>
			  <td><?php echo $nama;?></td>
			  <td><?php echo $mtk;?></td>
			  <td><?php echo $bing;?></td>
			  <td><?php echo $bindo;?></td>
			  <td><?php echo $ipa;?></td>
			  <td><?php echo $ips;?></td>
			  <td colspan="5"><?php  
               $hc1 = sqrt(pow(($mtk-$c1a),2)+pow(($bing-$c1b),2)+pow(($bindo-$c1c),2)+pow(($ipa-$c1d),2)+pow(($ips-$c1e),2));
                echo $hc1;
              ?></td>
              <td colspan="5"><?php 
                $hc2 = sqrt(pow(($mtk-$c2a),2)+pow(($bing-$c2b),2)+pow(($bindo-$c2c),2)+pow(($ipa-$c2d),2)+pow(($ips-$c2e),2));
                echo $hc2;
              ?></td>
			  <!--td bgcolor="<?php echo $warna1; ?>"><?php echo $tq->c1; ?></td>
			  <td bgcolor="<?php echo $warna2; ?>"><?php echo $tq->c2; ?></td-->
			  <?php
			  if($hc1<=$hc2)
              {
                  $arr_c1[$no] = 1;
              }
              else
              {
                $arr_c1[$no] = '0';
              }
              
              if($hc2<=$hc1)
              {
                  $arr_c2[$no] = 1;
              }
              else
              {
                $arr_c2[$no] = '0';
              }
			  ?>
			  <?php if($arr_c1[$no]==1){$warna1='#FFFF00';} else{$warna1='#ccc';} ?><td bgcolor="<?php echo $warna1; ?>"><?php echo $arr_c1[$no] ;?></td>
              <?php if($arr_c2[$no]==1){$warna2='#FFFF00';} else{$warna2='#ccc';} ?><td bgcolor="<?php echo $warna2; ?>"><?php echo $arr_c2[$no] ;?></td>
              
			  </tr>
              <?php
                }
              ?>
            </table>
            </div>
			
			
            <?php
              }
            ?>
            </div>


            <table  id="table_data" class="table table-bordered table-admin">
              <tr align="center">
			        <td>NIS</td>
			        <td>Nama Siswa</td>
			        <td>Penjurusan</td></tr>
              <?php
                foreach($data_hasil->result_array() as $h)
                {
					        $nis=$h['siswa_nis'];
					        $nama=$h['siswa_nama'];
					         $pnjr=$h['penjurusan'];
              ?>
              <tr align="center">
			        <td><?php echo $nis;?></td>
              <td><?php echo $nama; ?></td>
              <td><?php echo $pnjr; ?></td>
              </tr>
              <?php
                }
              ?>
            </table>
            </div>

            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div>
      </div>
    </div>
