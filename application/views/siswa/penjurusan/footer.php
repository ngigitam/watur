  <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo date('Y');?> by watur</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->
    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/dataTables.buttons.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/buttons.print.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/buttons.html5.min.js'?>"></script>
	<script src="<?php echo base_url().'assets/js/buttons.flash.min.js'?>"></script>
    <script type="text/javascript">
    
            //$('#mydata').DataTable();
			
		
    </script>
	<script type="text/javascript">
	    $(document).ready(function() {
			
            $(".perhitungan").on();
			var mtk=$('#mtk').val();
                var bing=$('#bing').val();
				var bindo=$('#bindo').val();
				var ipa=$('#ipa').val();
				var ips=$('#ips').val();
				var hasil = Math.sqrt(parseInt(Math.pow((mtk-75),2))+parseInt(Math.pow((bing-76),2))+parseInt(Math.pow((bindo-75),2))+parseInt(Math.pow((ipa-80),2))+parseInt(Math.pow((ips-71),2)));
				var hasil2 = Math.sqrt(parseInt(Math.pow((mtk-83),2))+parseInt(Math.pow((bing-85),2))+parseInt(Math.pow((bindo-86),2))+parseInt(Math.pow((ipa-76),2))+parseInt(Math.pow((ips-84),2)));            
			  $('#hc1').html(parseFloat(hasil).toFixed(4));
			  $('#hc2').html(parseFloat(hasil2).toFixed(4));
			  if (hasil<=hasil2)
			  {
				  $('#arr_c1').html(1);
				  document.getElementById('warna1').bgColor='#FFFF00';
				  $('#jurusan').html('IPA');
				}
			  else
			  {
				  $('#arr_c1').html(0);
				  document.getElementById('warna1').bgColor='#ccc';
			  }
			  if (hasil2<=hasil)
			  {
				  $('#arr_c2').html(1);
				  document.getElementById('warna2').bgColor='#FFFF00';
				  $('#jurusan').html('IPS');
				  
			  }
			  else
			  {
				  $('#arr_c2').html(0);
				  document.getElementById('warna2').bgColor='#ccc';
			  }
        } );

        $(function(){
			
            $(".perhitungan").on("input",function(){
                var mtk=$('#mtk').val();
                var bing=$('#bing').val();
				var bindo=$('#bindo').val();
				var ipa=$('#ipa').val();
				var ips=$('#ips').val();
				var hasil = Math.sqrt(parseInt(Math.pow((mtk-75),2))+parseInt(Math.pow((bing-76),2))+parseInt(Math.pow((bindo-75),2))+parseInt(Math.pow((ipa-80),2))+parseInt(Math.pow((ips-71),2)));
				var hasil2 = Math.sqrt(parseInt(Math.pow((mtk-83),2))+parseInt(Math.pow((bing-85),2))+parseInt(Math.pow((bindo-86),2))+parseInt(Math.pow((ipa-76),2))+parseInt(Math.pow((ips-84),2)));
            
			  $('#hc1').html(parseFloat(hasil).toFixed(4));
			  $('#hc2').html(parseFloat(hasil2).toFixed(4));
			  if (hasil<=hasil2)
			  {
				  $('#arr_c1').html(1);
				  document.getElementById('warna1').bgColor='#FFFF00';
				  $('#jurusan').html('IPA');
				  
			  }
			  else
			  {
				  $('#arr_c1').html(0);
				  document.getElementById('warna1').bgColor='#ccc';
			  }
			  if (hasil2<=hasil)
			  {
				  $('#arr_c2').html(1);
				  document.getElementById('warna2').bgColor='#FFFF00';
				  $('#jurusan').html('IPS');
				  
			  }
			  else
			  {
				  $('#arr_c2').html(0);
				  document.getElementById('warna2').bgColor='#ccc';
			  }
			  
			  
            })
            
        });
		function printContent(el){
	var restorepage = document.body.innerHTML;
	
	var kop = document.getElementById("hasil-output");

    kop.innerHTML = "<img width='880px' height='113px' src='<?php echo base_url()."assets/img/kop1.png"?>'/>";
	var printcontent = document.getElementById(el).innerHTML;
	var image = document.getElementById("hasil-output").src="<?php echo base_url()."assets/img/kop1.png"?>";
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}

     
    </script>
    
</body>

</html>
