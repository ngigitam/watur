   
      <div class="row">
        <div class="col-md-12">
        <?php error_reporting(0); ?>
		  <button onclick="printContent('body')">Print</button>
		  <div id="body">
		  <center><div id="hasil-output"></div></center>
		  <h1>Nilai</h1>
            <div class="table-responsive">
            <table class="table table-bordered table-condensed" >
			 <thead>
                    <tr align="center">
                        <th>No</th>
                        <th>NIS</th>
                        <th>Nama</th>
						<th>Kelas</th>
						<th>Tahun Akademik</th>
                        <th>Nilai Matematika</th>
                        <th>Nilai B.Ing</th>
                        <th>Nilai B.Ind</th>
						<th>IPA</th>
                        <th>IPS</th>
                        <?php
							if($this->session->userdata('akses')=='1')
                        echo '<th style="width:140px;text-align:center;">Aksi</th>';
						?>
						
                    </tr>
             </thead>
                <tbody>
               
                <?php 
                    $no=0;
                    foreach ($tbl_nilai->result_array() as $a):
                        $no++;
                        $id=$a['nilai_id'];
                        $nis=$a['siswa_nis'];
                        $nama_siswa=$a['siswa_nama'];
						$kls=$a['siswa_kelas'];
						$thn=$a['siswa_thn_akd'];
                        $mtk=$a['mtk'];
                        $bing=$a['bing'];
                        $bindo=$a['bindo'];
                        $ipa=$a['ipa'];
						$ips=$a['ips'];
                ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $no;?></td>
                       <td><?php echo $nis;?></td>
                        <td><?php echo $nama_siswa;?></td>
						<td><?php echo $kls;?></td>
						<td><?php echo $thn;?></td>
                        <td><?php echo $mtk;?></td>
                        <td><?php echo $bing;?></td>
                        <td><?php echo $bindo;?></td>
                        <td><?php echo $ipa;?></td>
						 <td><?php echo $ips;?></td>
						 <?php
							if($this->session->userdata('akses')=='1')
                        echo '<td style="text-align:center;">
                            <a class="btn btn-xs btn-warning" href="#modalEdit'.$id.'" data-toggle="modal" title="Edit"><span class="fa fa-edit"></span> Edit</a>
                            
                        </td>';
						?>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
       
		 <!-- ============ MODAL EDIT =============== -->
        <?php
                    foreach ($tbl_nilai->result_array() as $a) {
                        $id=$a['nilai_id'];
						$siswa_id=$a['siswa_id'];
                        $nis=$a['siswa_nis'];
                        $nama_siswa=$a['siswa_nama'];
                        $mtk=$a['mtk'];
                        $bing=$a['bing'];
                        $bindo=$a['bindo'];
                        $ipa=$a['ipa'];
						$ips=$a['ips'];
					
                    ?>
                <div id="modalEdit<?php echo $id?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Edit Nilai</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'siswa/siswa/edit_nilai1'?>">
                        <div class="modal-body">
                            <input name="kode" type="hidden" value="<?php echo $id;?>">
					<div class="form-group">
                    <label class="control-label col-xs-3" >NIS</label>
					<div class="col-xs-9">
                    <select class="form-control" name="siswa_id" id="siswa_id" style="width:280px;" required disabled>
                        <?php foreach($siswa as $row){?>
						<option <?php if($row->siswa_id == $siswa_id){ echo 'selected="selected"'; } ?> value="<?php echo $row->siswa_id ?>"><?php echo $row->siswa_nis?> </option>
						<?php }?>
                    </select>
					</div>
				 </div>
                           
					<div class="form-group">
                        <label class="control-label col-xs-3" >Nilai Matematika</label>
                        <div class="col-xs-9">
                            <input name="mtk" class="form-control" type="number" min="75" max="100" style="width:70px;" value="<?php echo $mtk;?>" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nilai B. Inggris</label>
                        <div class="col-xs-9">
                            <input name="bing" class="form-control" type="number" min="75" max="100" style="width:70px;" value="<?php echo $bing;?>" required>
                        </div>
                    </div>
					
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nilai B. Indonesia</label>
                        <div class="col-xs-9">
                            <input name="bindo" class="form-control" type="number" min="75" max="100" style="width:70px;" value="<?php echo $bindo;?>" required>
                        </div>
                    </div>
                   <div class="form-group">
                        <label class="control-label col-xs-3" >IPA</label>
                        <div class="col-xs-9">
                            <input name="ipa" class="form-control" type="number" min="70" max="100" style="width:70px;" value="<?php echo $ipa;?>" required>
                        </div>
                    </div>
				<div class="form-group">
                        <label class="control-label col-xs-3" >IPS</label>
                        <div class="col-xs-9">
                            <input name="ips" class="form-control" type="number" min="70" max="100" style="width:70px;" value="<?php echo $ips;?>"  required>
                        </div>
				</div>

       </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

		  <!-- ============ hasil jurusan =============== -->
		  <h1>Tes Rekomendasi Penjurusan</h1>
          <div class="perhitungan">
            <br>
            <div class="table-responsive">
            <!--table class="table table-bordered table-admin" -->
			 <table class="table table-bordered table-condensed" >
			 <thead>
              <tr align="center">
			  <td rowspan="2">NIS</td>
			  <td rowspan="2">Nama Siswa</td>
			  <td rowspan="2">Nilai Matematika</td>
			  <td rowspan="2">Nilai B.Ing</td>
			  <td rowspan="2">Nilai B.Ind</td>
			  <td rowspan="2">IPA</td>
			  <td rowspan="2">IPS</td>
              <td colspan="5">Centroid 1</td>
			  <td colspan="5">Centroid 2</td>
			<td rowspan="2">C1</td>
			<td rowspan="2">C2</td>
              </tr>
              <tr align="center">
              <td>75</td><td>76</td><td>75</td><td>80</td><td>71</td>
              <td>83</td><td>85</td><td>86</td><td>76</td><td>84</td>
              </tr>
			  </thead>
                <tbody>
              <?php 
              $c1a = 75;
              $c1b = 76;
			  $c1c = 75;
			  $c1d = 80;
			  $c1e = 71;
              
              $c2a = 83;
              $c2b = 85;
			  $c2c = 86;
			  $c2d = 76;
			  $c2e = 84;

              foreach($tbl_nilai->result_array() as $s){ ?>
              <tr>
			  <?php $s['siswa_id']; ?>
			  <td style="text-align:center;"><?php echo $s['siswa_nis']; ?></td>
			  <td><?php echo $s['siswa_nama']; ?></td>
			  <td style="text-align:center;">
			  <input name="mtk" id="mtk" class="form-control" type="number" min="75" max="100" style="width:67px;" value="<?php echo $s['mtk'];?>" required>
			  </td>
			  <td style="text-align:center;">
			  <input name="bing" id="bing" class="form-control" type="number" min="75" max="100" style="width:67px;" value="<?php echo $s['bing'];?>" required>
			  </td>
			  <td style="text-align:center;">
			  <input name="bindo" id="bindo" class="form-control" type="number" min="75" max="100" style="width:67px;" value="<?php echo $s['bindo'];?>" required>
			  </td>
			  <td style="text-align:center;">
			  <input name="ipa" id="ipa" class="form-control" type="number" min="70" max="100" style="width:67px;" value="<?php echo $s['ipa'];?>" required>
			  </td>
			  <td style="text-align:center;">
			  <input name="ips" id="ips" class="form-control" type="number" min="70" max="100" style="width:67px;" value="<?php echo $s['ips'];?>" required>
			  </td>
				<td colspan="5" style="text-align:center;"> 
				<div id="hc1"></div>
			  </td>
              <td colspan="5" style="text-align:center;">
			  <div id="hc2"></div>
			  </td>
			  <td id="warna1" style="text-align:center;"><div id="arr_c1"></div></td>
			  <td id="warna2" style="text-align:center;"><div id="arr_c2"></div></td>  
			  </tr>
              <?php
              $no++; }            
              ?>
			   </tbody>
            </table>
            </div>
            </div>
	
			
			<!--table class="table table-bordered table-admin"-->
			<table class="table table-bordered table-condensed" >
			 <thead>
              <tr align="center">
			        <th>NIS</th>
			        <th>Nama Siswa</th>
			        <th>Penjurusan</th>
			</tr>
			 </thead>
                <tbody>
					
              <?php
                foreach($tbl_nilai->result_array() as $h)
                {
					        $nis=$h['siswa_nis'];
					        $nama=$h['siswa_nama'];
					         
              ?>
              <tr>
			  <td><?php echo $nis;?></td>
              <td><?php echo $nama; ?></td>
			  <td><div id="jurusan"></div></td>
              </tr>
              <?php
                }
              ?>
			  </tbody>
            </table>
			        
			
          
			
			</div>
            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div>
      </div>

