<?php

class Welcome extends CI_Controller {
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('');
            redirect($url);
        };

	}
	
	function index(){
	if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='3' || $this->session->userdata('akses')=='4' || $this->session->userdata('akses')=='5' || $this->session->userdata('akses')=='6'){
		$this->load->view('admin/v_index','');
	}else{
        //echo "Halaman tidak ditemukan";
        redirect('administrator');
    }
	}
}
