<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_siswa');
		$this->load->model('m_penjurusan');
	}
	function index(){
	if($this->session->userdata('akses')=='3'){
		//$data['data']=$this->m_siswa->get_siswa();
		//$ids=$this->session->set_userdata('idsiswa',$siswa_id);
		$ids=$this->session->userdata('nis');
		$data['data']=$this->m_siswa->get_siswa1($ids);
		$this->load->view('siswa/v_siswa',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function edit_siswa(){
	if($this->session->userdata('akses')=='3'){
		$kode=$this->input->post('kode');
		$nis=$this->input->post('nis');
		$nama_siswa=$this->input->post('nama');
		//$jk=$this->input->post('jk');
		$alt=$this->input->post('alt');
		$kls=$this->input->post('kls');
		$thn=$this->input->post('thn');
		//$status=$this->input->post('status');
		$password=$this->input->post('password');
		$password2=$this->input->post('password2');
		if (empty($password) && empty($password2)) {
			$this->m_siswa->update_siswa_nopass1($kode,$nis,$nama_siswa,$kls,$thn);
			echo $this->session->set_flashdata('msg','<label class="label label-success">siswa Berhasil diupdate</label>');
			redirect('siswa/siswa');
		}elseif ($password2 <> $password) {
			echo $this->session->set_flashdata('msg','<label class="label label-danger">Password yang Anda Masukan Tidak Sama</label>');
			redirect('siswa/siswa');
		}else{
			$this->m_siswa->update_siswa1($kode,$nis,$nama_siswa,$kls,$thn,$password);
			echo $this->session->set_flashdata('msg','<label class="label label-success">siswa Berhasil diupdate</label>');
			redirect('siswa/siswa');
		}
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function penjurusan(){
	if($this->session->userdata('akses')=='3'){
		$ids=$this->session->userdata('nis');
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_penjurusan($hmax,$ids);
		$this->load->view('siswa/v_penjurusan_siswa',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	
	function nilai_uji(){
	if($this->session->userdata('akses')=='3'){
		$data['siswa'] = $this->m_siswa->get_siswa2()->result();
		$ids=$this->session->userdata('nis');
		$data['data'] = $this->m_siswa->get_nilai1($ids);
		$this->load->view('siswa/v_nilai_uji',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	
	function edit_nilai1(){
	if($this->session->userdata('akses')=='3'){
		$kode=$this->input->post('kode');
	    //$siswa_id=$this->input->post('siswa_id');
		$mtk=$this->input->post('mtk');
		$bing=$this->input->post('bing');
		$bindo=$this->input->post('bindo');
		$ipa=$this->input->post('ipa');
		$ips=$this->input->post('ips');
		$this->m_siswa->update_nilai1($kode,$mtk,$bing,$bindo,$ipa,$ips);
		echo $this->session->set_flashdata('msg','<label class="label label-success">nilai Berhasil diupdate</label>');
		redirect('siswa/siswa/iterasi_kmeans');
		
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	
	function lap_data_penjurusan(){
	if($this->session->userdata('akses')=='3'){
		$ids=$this->session->userdata('nis');
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_penjurusan($hmax,$ids);
		$this->load->view('siswa/v_lap_penjurusan_siswa',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	
	function iterasi_kmeans(){
		if($this->session->userdata('akses')=='3'){
		
		   	
			$ids=$this->session->userdata('nis');
			$tbl_nilai = $this->m_siswa->selectdata1('tbl_nilai',$ids);
			$data = array(
				'tbl_nilai'=> $tbl_nilai,
			);
			$data['siswa'] = $this->m_siswa->get_siswa2()->result();
			$this->load->view('siswa/penjurusan/header',$data);
			$this->load->view('siswa/penjurusan/iterasi_kmeans');
			$this->load->view('siswa/penjurusan/footer');
		}else{
        echo "Halaman tidak ditemukan";
		}
	}
}