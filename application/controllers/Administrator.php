<?php
class Administrator extends CI_Controller{
    function __construct(){
        parent:: __construct();
        $this->load->model('m_login');
    }
    function index(){
        $x['judul']="Silahkan Masuk..!";
        $this->load->view('admin/v_login',$x);
    }
    function cekuser(){
        $username=strip_tags(str_replace("'", "", $this->input->post('username')));
        $password=strip_tags(str_replace("'", "", $this->input->post('password')));
        $u=$username;
        $p=$password;
        $cadmin=$this->m_login->cekadmin($u,$p);
        echo json_encode($cadmin);
        if($cadmin->num_rows() > 0){
         $this->session->set_userdata('masuk',true);
         $this->session->set_userdata('user',$u);
         $xcadmin=$cadmin->row_array();
         if($xcadmin['user_level']=='1'){
            $this->session->set_userdata('akses','1');
            $idadmin=$xcadmin['user_id'];
            $user_nama=$xcadmin['user_nama'];
            $this->session->set_userdata('idadmin',$idadmin);
            $this->session->set_userdata('nama',$user_nama);
            redirect('welcome');
          }elseif ($xcadmin['user_level']=='2'){
             $this->session->set_userdata('akses','2');
             $idadmin=$xcadmin['user_id'];
             $user_nama=$xcadmin['user_nama'];
             $this->session->set_userdata('idadmin',$idadmin);
             $this->session->set_userdata('nama',$user_nama);
             redirect('welcome');
          }elseif ($xcadmin['user_level']=='4'){
             $this->session->set_userdata('akses','4');
             $idadmin=$xcadmin['user_id'];
             $user_nama=$xcadmin['user_nama'];
             $this->session->set_userdata('idadmin',$idadmin);
             $this->session->set_userdata('nama',$user_nama);
             redirect('welcome');
           }elseif ($xcadmin['user_level']=='5'){
             $this->session->set_userdata('akses','5');
             $idadmin=$xcadmin['user_id'];
             $user_nama=$xcadmin['user_nama'];
             $this->session->set_userdata('idadmin',$idadmin);
             $this->session->set_userdata('nama',$user_nama);
             redirect('welcome');
		   }elseif ($xcadmin['user_level']=='6'){
             $this->session->set_userdata('akses','6');
             $idadmin=$xcadmin['user_id'];
             $user_nama=$xcadmin['user_nama'];
             $this->session->set_userdata('idadmin',$idadmin);
             $this->session->set_userdata('nama',$user_nama);
             redirect('welcome');
		   }

        }elseif ($cadmin->num_rows() == 0){
         $cadmin=$this->m_login->ceksiswa($u,$p);
         echo json_encode($cadmin);
         $this->session->set_userdata('masuk',true);
         $this->session->set_userdata('user',$u);
         $xcsiswa=$cadmin->row_array();
         if($xcsiswa['siswa_level']=='3'){
            $this->session->set_userdata('akses','3');
            $id_siswa=$xcsiswa['siswa_id'];
            $siswa_nis=$xcsiswa['siswa_nis'];
            $this->session->set_userdata('idsiswa',$id_siswa);
            $this->session->set_userdata('nis',$siswa_nis);
            redirect('welcome');
         }else{
             echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Username Atau Password Salah</div>');
             redirect('administrator');
         }
         
       }else{
         echo $this->session->set_flashdata('msg','<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert"><span class="fa fa-close"></span></button> Username Atau Password Salah</div>');
         redirect('administrator');
       }
       
    }
        function logout(){
            $this->session->sess_destroy();
            $url=base_url('administrator');
            redirect($url);
        }
}