<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjurusan extends CI_Controller {
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_penjurusan');
	}

	
	function index(){

	if($this->session->userdata('akses')=='1'){
			$tbl_nilai = $this->m_penjurusan->selectdata('tbl_nilai');

			$data = array(
				'tbl_nilai'=> $tbl_nilai,
			);
			$this->load->view('admin/penjurusan/header',$data);
			$this->load->view('admin/penjurusan/generate_awal');
			$this->load->view('admin/penjurusan/footer');

		}else{
			echo "Halaman tidak ditemukan";
		}
	}
	
	function generate_awal(){

	if($this->session->userdata('akses')=='1'){
			//$this->load->model('model');
			$tbl_nilai = $this->m_penjurusan->selectdata('tbl_nilai');

			$data = array(
				'tbl_nilai'=> $tbl_nilai,
			);
			$this->load->view('admin/penjurusan/header',$data);
			$this->load->view('admin/penjurusan/generate_awal');
			$this->load->view('admin/penjurusan/footer');

		}else{
			echo "Halaman tidak ditemukan";
		}
	}


	function generate_rata(){
		if($this->session->userdata('akses')=='1'){

		$tbl_nilai = $this->db->get('tbl_nilai');
		$v = "";
		if(count($tbl_nilai->result())<0)
		{
			$nilai = floor(($s->mtk+$s->bing+$s->bindo+$s->ipa+$s->ips)/5);
			$v = "insert into rata_rata (siswa_id,rata_rata) values ('".$s->siswa_id."','".$nilai."')";
			$this->db->query($v);
		}
		else
		{
			$this->db->query('truncate table rata_rata');
			foreach($tbl_nilai->result() as $s)
			{
				$nilai = floor(($s->mtk+$s->bing+$s->bindo+$s->ipa+$s->ips)/5);
				$v = "insert into rata_rata (siswa_id,rata_rata) values ('".$s->siswa_id."','".$nilai."')";
				$this->db->query($v);
			}
		}
		
		$data['tbl_nilai'] = $this->db->query('select * from tbl_nilai left join rata_rata on tbl_nilai.siswa_id=rata_rata.siswa_id left join tbl_siswa on tbl_nilai.siswa_id=tbl_siswa.siswa_id ');

		$this->load->view('admin/penjurusan/header',$data);
		$this->load->view('admin/penjurusan/generate_rata');
		$this->load->view('admin/penjurusan/footer');
		}else{
        echo "Halaman tidak ditemukan";
		}
	}


	function generate_centroid(){
		if($this->session->userdata('akses')=='1'){

		$kluster = 2;
		//81-100 = sangat baik
		//70-80 = baik
		$data['c1'] = rand(81,100);
		$data['c2'] = rand(70,80);
		$tbl_nilai = $this->db->query('select * from tbl_nilai left join rata_rata on tbl_nilai.siswa_id=rata_rata.siswa_id');
		$st = "";
		
		$this->db->query('truncate table hasil');
		foreach($tbl_nilai->result() as $s)
		{
			$d1 = abs($s->rata_rata-$data['c1']); //96-90 = 6
			$d2 = abs($s->rata_rata-$data['c2']); // 78 - 75 = 3
			
			$array_sort_awal = array($d1,$d2);
			$array_sort = $array_sort_awal;
			for ($j=1;$j<=$kluster-1;$j++){//1 4 --> 2
				for ($k=0;$k<=$kluster-2;$k++) {//0 2 --> 1
					if ($array_sort[$k] > $array_sort[$k + 1]){ // $array_sort[0] > $array_sort[1] --> 6 > 3
						$temp = $array_sort[$k]; // 3
						$array_sort[$k] = $array_sort[$k + 1]; // 4
						$array_sort[$k + 1] = $temp; //$array_sort[1] = 4
					}
				}
			}
			
			for ($i = 0; $i < $kluster; $i++){
				for($r = 0; $r < $kluster; $r++)
				{
					if($array_sort[0]==$array_sort_awal[$r])
					{
						if($r==0) $st =  "IPA";
						else if($r==1) $st =  "IPS";
					}
				}
			}
			$this->db->query("insert into hasil (siswa_id,predikat,d1,d2) values('".$s->siswa_id."','".$st."','".$d1."','".$d2."')");
		}

		$data['tbl_nilai'] = $this->db->query("select * from tbl_nilai left join (rata_rata,hasil) on tbl_nilai.siswa_id=rata_rata.siswa_id and tbl_nilai.siswa_id=hasil.siswa_id join tbl_siswa on tbl_nilai.siswa_id=tbl_siswa.siswa_id");

		$this->load->view('admin/penjurusan/header',$data);
		$this->load->view('admin/penjurusan/generate_centroid');
		$this->load->view('admin/penjurusan/footer');
		}else{
        echo "Halaman tidak ditemukan";
		}
	}

	function iterasi_kmeans(){
		if($this->session->userdata('akses')=='1'){
			$tbl_nilai = $this->m_penjurusan->selectdata('tbl_nilai');
			

			$data = array(
				'tbl_nilai'=> $tbl_nilai,
			);
			$data['c'] = $this->db->query('select mtk,bing,bindo,ipa,ips from tbl_pengaturan ORDER BY RAND()');
			$data['c1'] = $this->db->query('select mtk,bing,bindo,ipa,ips from tbl_pengaturan where pengaturan_id=1');
			$data['c2'] = $this->db->query('select mtk,bing,bindo,ipa,ips from tbl_pengaturan where pengaturan_id=2');
			$this->load->view('admin/penjurusan/header',$data);
			$this->load->view('admin/penjurusan/iterasi_kmeans');
			$this->load->view('admin/penjurusan/footer');
		}else{
        echo "Halaman tidak ditemukan";
		}
	}
	
	function iterasi_kmeans_lanjut(){
		if($this->session->userdata('akses')=='1'){
			

		$data['tbl_nilai'] = $this->db->query("select * from tbl_nilai left join tbl_siswa on tbl_nilai.siswa_id=tbl_siswa.siswa_id");

		$id = "";
		$id = $this->db->query('select max(nomor) as m from hasil_centroid');
		foreach($id->result() as $i)
		{
			$id = $i->m;
		}
		$this->db->where('nomor', $id);
		$data['centroid'] = $this->db->get('hasil_centroid');
		$data['id'] = $id+1;
		
		$it = "";
		$it = $this->db->query('select max(iterasi) as it from centroid_temp');
		foreach($it->result() as $i)
		{
			$it = $i->it;
		}
		$it_temp = $it-1;
		$this->db->where('iterasi', $it_temp);
		$it_sebelum = $this->db->get('centroid_temp');
		$c1_sebelum = array();
		$c2_sebelum = array();
		//$c2_sebelum = array();
		$no=0;
		foreach($it_sebelum->result() as $it_prev)
		{
			$c1_sebelum[$no] = $it_prev->c1;
			$c2_sebelum[$no] = $it_prev->c2;
			//$c3_sebelum[$no] = $it_prev->c3;
			$no++;
		}
		
		$this->db->where('iterasi', $it);
		$it_sesesudah = $this->db->get('centroid_temp');
		$c1_sesesudah = array();
		$c2_sesesudah = array();
		//$c2_sebelum = array();
		$no=0;
		foreach($it_sesesudah->result() as $it_next)
		{
			$c1_sesesudah[$no] = $it_next->c1;
			$c2_sesesudah[$no] = $it_next->c2;
			//$c3_sesesudah[$no] = $it_next->c3;
			$no++;
		}
		
		if($c1_sebelum==$c1_sesesudah || $c2_sebelum==$c2_sesesudah)
		{
			?>
				<script>
					alert("Proses iterasi berakhir pada tahap ke-<?php echo $it; ?>");
				</script>
			<?php
				echo "<meta http-equiv='refresh' content='0; url=".base_url()."admin/penjurusan/iterasi_kmeans_hasil'>";

		}
		else
		{
			$data['q'] = $this->db->query('select * from centroid_temp group by iterasi');
			
			$this->load->view('admin/penjurusan/header',$data);
			$this->load->view('admin/penjurusan/iterasi_kmeans_lanjut');
			$this->load->view('admin/penjurusan/footer');
		}
		}else{
        echo "Halaman tidak ditemukan";
		}
	}	

	function iterasi_kmeans_hasil(){
		if($this->session->userdata('akses')=='1'){
		
		$data['tbl_nilai'] = $this->db->query("select * from tbl_nilai left join tbl_siswa on tbl_nilai.siswa_id=tbl_siswa.siswa_id");
				$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
				foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
				$data_hasil=$this->db->query("SELECT siswa_nis, siswa_nama,(if(c1=1,'IPA',if(c2=1,'IPS','')))as penjurusan FROM tbl_siswa INNER JOIN centroid_temp ON tbl_siswa.siswa_id=centroid_temp.siswa_id WHERE iterasi='".$hmax."'");
				
					$data = array(
						'data_hasil'	=> $data_hasil
					);

				$data['q'] = $this->db->query('select * from centroid_temp where iterasi is not null and iterasi group by iterasi');

			$this->load->view('admin/penjurusan/header',$data);
			$this->load->view('admin/penjurusan/iterasi_kmeans_hasil');
			$this->load->view('admin/penjurusan/footer');
		}else{
        echo "Halaman tidak ditemukan";
		}
	}
	function iterasi_kmeans_hasil1(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
		
		$data['tbl_nilai'] = $this->db->query("select * from tbl_nilai left join tbl_siswa on tbl_nilai.siswa_id=tbl_siswa.siswa_id");
		$tbl_nilai = $this->m_penjurusan->selectdata('tbl_nilai');
				$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
				foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
				$data_hasil=$this->db->query("SELECT siswa_nis, siswa_nama,mtk,bing,bindo,ipa,ips,(if(c1=1,'IPA',if(c2=1,'IPS','')))as penjurusan FROM tbl_siswa INNER JOIN centroid_temp ON tbl_siswa.siswa_id=centroid_temp.siswa_id LEFT JOIN tbl_nilai ON tbl_siswa.siswa_id=tbl_nilai.siswa_id WHERE iterasi='".$hmax."'");
				
					$data = array(
						'data_hasil'	=> $data_hasil
					);
			$data['c'] = $this->db->query('select * from hasil_centroid where nomor=0 ');
				$data['q'] = $this->db->query('select * from centroid_temp group by iterasi');
				$data['q1'] = $this->db->query('select * from centroid_temp where iterasi is not null and iterasi group by iterasi')->result();
			$this->load->view('admin/penjurusan/header',$data);
			$this->load->view('admin/penjurusan/iterasi_kmeans_hasil1');
			$this->load->view('admin/penjurusan/footer');
		}else{
        echo "Halaman tidak ditemukan";
		}
	}

	function hasil_topsis(){
		if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2'){
		
		$data['tbl_nilai'] = $this->db->query("select * from tbl_nilai left join tbl_siswa on tbl_nilai.siswa_id=tbl_siswa.siswa_id");
		$tbl_nilai = $this->m_penjurusan->selectdata('tbl_nilai');
				$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
				foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
				$data_hasil=$this->db->query("SELECT siswa_nis,siswa_nama,(mtk+bing+bindo+ipa+ips) AS nilai,(if(c1=1,'IPA',if(c2=1,'IPS','')))as penjurusan 
				FROM tbl_siswa INNER JOIN centroid_temp ON tbl_siswa.siswa_id=centroid_temp.siswa_id 
				LEFT JOIN tbl_nilai ON tbl_siswa.siswa_id=tbl_nilai.siswa_id 
				WHERE iterasi='".$hmax."' AND c1=1
				ORDER BY
				nilai DESC
				");
								$data_hasil1=$this->db->query("SELECT siswa_nis,siswa_nama,(mtk+bing+bindo+ipa+ips) AS nilai,(if(c1=1,'IPA',if(c2=1,'IPS','')))as penjurusan 
								FROM tbl_siswa INNER JOIN centroid_temp ON tbl_siswa.siswa_id=centroid_temp.siswa_id 
								LEFT JOIN tbl_nilai ON tbl_siswa.siswa_id=tbl_nilai.siswa_id 
								WHERE iterasi='".$hmax."' AND c2=1
								ORDER BY
								nilai DESC
								");
				
					$data = array(
						'data_hasil'	=> $data_hasil,
						'data_hasil1'	=> $data_hasil1
					);
			$data['c'] = $this->db->query('select * from hasil_centroid where nomor=0 ');
				$data['q'] = $this->db->query('select * from centroid_temp group by iterasi');
				$data['q1'] = $this->db->query('select * from centroid_temp where iterasi is not null and iterasi group by iterasi')->result();
			$this->load->view('admin/penjurusan/header',$data);
			$this->load->view('admin/penjurusan/v_hasil_topsis');
			$this->load->view('admin/penjurusan/footer');
		}else{
        echo "Halaman tidak ditemukan";
		}
	}
	
	public function view_iterasi()
	{
		if (isset($_POST['cari'])) {
			$data['v_data'] = $this->db->query('select * from centroid_temp where iterasi='.$this->input->post('iterasi').' group by iterasi');
			$this->load->view('admin/penjurusan/iterasi_kmeans_hasil2',$data);
		}else {
			echo "Halaman tidak ditemukan";
		}
	}
	
	function hasil_penjurusan(){
	if($this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4'){
		$ids=$this->session->userdata('nis');
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_penjurusan->get_hasil_penjurusan($hmax);
		$this->load->view('siswa/v_penjurusan_siswa',$data);
	}elseif($this->session->userdata('akses')=='5'){
		$ids=$this->session->userdata('nis');
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_penjurusan->get_hasil_penjurusan1($hmax);
		$this->load->view('siswa/v_penjurusan_siswa',$data);
	}elseif($this->session->userdata('akses')=='6'){
		$ids=$this->session->userdata('nis');
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_penjurusan->get_hasil_penjurusan2($hmax);
		$this->load->view('siswa/v_penjurusan_siswa',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	




}