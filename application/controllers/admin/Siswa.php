<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_siswa');
	}
	function index(){
	if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4'){
		$data['data']=$this->m_siswa->get_siswa();
		$this->load->view('admin/v_siswa',$data);
	}elseif($this->session->userdata('akses')=='5'){	
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ak($hmax);
			$this->load->view('admin/v_siswa',$data);
	}elseif($this->session->userdata('akses')=='6'){	
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ap($hmax);
			$this->load->view('admin/v_siswa',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function tambah_siswa(){
	if($this->session->userdata('akses')=='1'){
	    $nis=$this->input->post('nis');
		$nama_siswa=$this->input->post('nama');
		$jk=$this->input->post('jk');
		$kls=$this->input->post('kls');
		$thn=$this->input->post('thn');
		$password=$this->input->post('password');
		$password2=$this->input->post('password2');
		
		if ($password2 <> $password) {
			echo $this->session->set_flashdata('msg','<label class="label label-danger">Password yang Anda Masukan Tidak Sama</label>');
			redirect('admin/siswa');
		}else{
			$this->m_siswa->simpan_siswa($nis,$nama_siswa,$jk,$kls,$thn,$password,$level);
			echo $this->session->set_flashdata('msg','<label class="label label-success">siswa Berhasil ditambahkan</label>');
			redirect('admin/siswa');
		}
		
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function tambah_siswa1(){
	if($this->session->userdata('akses')=='1'){
	$this->_set_rules();
    if ($this->form_validation->run() == true) {
		$cek = $this->db->query("SELECT * FROM tbl_siswa where siswa_nis='".$this->input->post('nis')."'")->num_rows();
        if ($cek<=0) {          
            $data = array(
			'siswa_nis'=> $this->input->post('nis'),
			'siswa_nama'=> $this->input->post('nama'),
			'siswa_jk'=>$this->input->post('jk'),
			'siswa_kelas'=>$this->input->post('kls'),
			'siswa_thn_akd'=>$this->input->post('thn'),
			'siswa_password'=>$this->input->post('password')
			);

			$password=$this->input->post('password');
			$password2=$this->input->post('password2');
			if ($password2 <> $password) {
			echo $this->session->set_flashdata('msg','<label class="label label-danger">Password yang Anda Masukan Tidak Sama</label>');
			redirect('admin/siswa');
			}else{
			$this->m_siswa->simpan($data);
			echo $this->session->set_flashdata('msg','<label class="label label-success">siswa Berhasil ditambahkan</label>');
			redirect('admin/siswa');
			}
        }
		echo $this->session->set_flashdata('msg','<label class="label label-danger">NIS sudah ada</label>');
		redirect('admin/siswa');
		}else{
			redirect('admin/siswa');
		}
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function _set_rules() {
        $this->form_validation->set_rules('nis', 'NIS', 'required');
    }
	
	function edit_siswa(){
	if($this->session->userdata('akses')=='1'){
		$kode=$this->input->post('kode');
	    $nis=$this->input->post('nis');
		$nama_siswa=$this->input->post('nama');
		$jk=$this->input->post('jk');
		$kls=$this->input->post('kls');
		$thn=$this->input->post('thn');
		$password=$this->input->post('password');
		$password2=$this->input->post('password2');
		$status=$this->input->post('status');
		if (empty($password) && empty($password2)) {
			$this->m_siswa->update_siswa_nopass($kode,$nis,$nama_siswa,$jk,$kls,$thn,$status);
			echo $this->session->set_flashdata('msg','<label class="label label-success">siswa Berhasil diupdate</label>');
			redirect('admin/siswa');
		}elseif ($password2 <> $password) {
			echo $this->session->set_flashdata('msg','<label class="label label-danger">Password yang Anda Masukan Tidak Sama</label>');
			redirect('admin/siswa');
		}else{
			$this->m_siswa->update_siswa($kode,$nis,$nama_siswa,$jk,$kls,$thn,$password,$status);
			echo $this->session->set_flashdata('msg','<label class="label label-success">siswa Berhasil diupdate</label>');
			redirect('admin/siswa');
		}
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	
	function hapus_siswa(){
	if($this->session->userdata('akses')=='1'){
		$kode=$this->input->post('kode');
		$this->m_siswa->hapus_siswa($kode);
		redirect('admin/siswa');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
}