<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_mapel');
	}
	function index(){
	if($this->session->userdata('akses')=='1'){
		$data['data']=$this->m_mapel->get_mapel();
		$this->load->view('admin/v_mapel',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function tambah_mapel(){
	if($this->session->userdata('akses')=='1'){
	    $kdm=$this->input->post('kd_mapel');
		$nmm=$this->input->post('nama_mapel');
		$this->m_mapel->simpan_mapel($kdm,$nmm);
		echo $this->session->set_flashdata('msg','<label class="label label-success">mapel Berhasil ditambahkan</label>');
		redirect('admin/mapel');
		
		
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function edit_mapel(){
	if($this->session->userdata('akses')=='1'){
		$kdm=$this->input->post('kd_mapel');
		$nmm=$this->input->post('nama_mapel');
		$this->m_mapel->update_mapel($kdm,$nmm);
		echo $this->session->set_flashdata('msg','<label class="label label-success">mapel Berhasil diupdate</label>');
		redirect('admin/mapel');
		
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	
	function hapus_mapel(){
	if($this->session->userdata('akses')=='1'){
		$kdm=$this->input->post('kd_mapel');
		$this->m_mapel->hapus_mapel($kdm);
		redirect('admin/mapel');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
}