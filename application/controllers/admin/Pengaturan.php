<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_pengaturan');
	}
	function index(){
	if($this->session->userdata('akses')=='1'){
		$data['data']=$this->m_pengaturan->get_pengaturan();
		$this->load->view('admin/v_pengaturan',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function tambah_pengaturan(){
	if($this->session->userdata('akses')=='1'){
	    $nmj=$this->input->post('nama_jurusan');
		$mtk=$this->input->post('mtk');
		$bing=$this->input->post('bing');
		$bindo=$this->input->post('bindo');
		$ipa=$this->input->post('ipa');
		$ips=$this->input->post('ips');
		$this->m_pengaturan->simpan_pengaturan($nmj,$mtk,$bing,$bindo,$ipa,$ips);
		echo $this->session->set_flashdata('msg','<label class="label label-success">pengaturan Berhasil ditambahkan</label>');
		redirect('admin/pengaturan');
				
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function edit_pengaturan(){
	if($this->session->userdata('akses')=='1'){
		$kode=$this->input->post('kode');
	    $nmj=$this->input->post('nama_jurusan');
		$mtk=$this->input->post('mtk');
		$bing=$this->input->post('bing');
		$bindo=$this->input->post('bindo');
		$ipa=$this->input->post('ipa');
		$ips=$this->input->post('ips');
		$this->m_pengaturan->update_pengaturan($kode,$nmj,$mtk,$bing,$bindo,$ipa,$ips);
		echo $this->session->set_flashdata('msg','<label class="label label-success">pengaturan Berhasil diupdate</label>');
		redirect('admin/pengaturan');
		
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	
	function hapus_pengaturan(){
	if($this->session->userdata('akses')=='1'){
		$kode=$this->input->post('kode');
		$this->m_pengaturan->hapus_pengaturan($kode);
		redirect('admin/pengaturan');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
}