<?php
class Laporan extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_siswa');
		$this->load->model('m_nilai');
		$this->load->model('m_penjurusan');
		$this->load->model('m_laporan');
	}
	function index(){
	if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
		foreach($lap->result_array() as $h)
		{
			$hmax=$h['maksimal'];
		}
		$data['data'] = $this->m_laporan->get_data($hmax);
		$this->load->view('admin/v_laporan',$data);
	}elseif($this->session->userdata('akses')=='5'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ak($hmax);
			$this->load->view('admin/v_laporan',$data);
	}elseif($this->session->userdata('akses')=='6'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ap($hmax);
			$this->load->view('admin/v_laporan',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function lap_data_siswa(){
	if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4'){
		$data['data']=$this->m_siswa->get_siswa();
		$this->load->view('admin/laporan/v_lap_siswa',$data);
	}elseif($this->session->userdata('akses')=='5'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ak($hmax);
			$this->load->view('admin/laporan/v_lap_siswa',$data);
	}elseif($this->session->userdata('akses')=='6'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ap($hmax);
			$this->load->view('admin/laporan/v_lap_siswa',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function lap_data_nilai(){
	if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4'){
		$data['siswa'] = $this->m_nilai->get_siswa();
		$data['data']=$this->m_nilai->get_nilai();
		$this->load->view('admin/laporan/v_lap_nilai',$data);
	}elseif($this->session->userdata('akses')=='5'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ak($hmax);
			$this->load->view('admin/laporan/v_lap_nilai',$data);
	}elseif($this->session->userdata('akses')=='6'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ap($hmax);
			$this->load->view('admin/laporan/v_lap_nilai',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function lap_data_penjurusan(){
	if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_laporan->get_data($hmax);
		$this->load->view('admin/laporan/v_lap_penjurusan',$data);
	}elseif($this->session->userdata('akses')=='5'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_penjurusan->get_hasil_penjurusan1($hmax);
			$this->load->view('admin/laporan/v_lap_penjurusan',$data);
	}elseif($this->session->userdata('akses')=='6'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_penjurusan->get_hasil_penjurusan2($hmax);
			$this->load->view('admin/laporan/v_lap_penjurusan',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	
	function lap_semua_data(){
	if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
		$data['data'] = $this->m_penjurusan->get_hasil_penjurusan($hmax);
		$this->load->view('admin/laporan/v_lap_semua',$data);
	}elseif($this->session->userdata('akses')=='5'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_penjurusan->get_hasil_penjurusan1($hmax);
			$this->load->view('admin/laporan/v_lap_semua',$data);
	}elseif($this->session->userdata('akses')=='6'){
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_penjurusan->get_hasil_penjurusan2($hmax);
			$this->load->view('admin/laporan/v_lap_semua',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
}