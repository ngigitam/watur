<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_nilai');
		$this->load->model('m_siswa');
	}
	function index(){
	if($this->session->userdata('akses')=='1' || $this->session->userdata('akses')=='2' || $this->session->userdata('akses')=='4'){
		$data['siswa'] = $this->m_nilai->get_siswa()->result();
		$data['data']=$this->m_nilai->get_nilai();
		$this->load->view('admin/v_nilai',$data);
	}elseif($this->session->userdata('akses')=='5'){	
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ak($hmax);
			$this->load->view('admin/v_nilai',$data);
	}elseif($this->session->userdata('akses')=='6'){	
		$lap= $this->db->query('SELECT MAX(iterasi) AS maksimal FROM centroid_temp');
			foreach($lap->result_array() as $h)
				$hmax=$h['maksimal'];
			$data['data'] = $this->m_siswa->get_siswa_ap($hmax);
			$this->load->view('admin/v_nilai',$data);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}

	function tambah_nilai(){
	if($this->session->userdata('akses')=='1'){
	    $siswa_id=$this->input->post('siswa_id');
		$mtk=$this->input->post('mtk');
		$bing=$this->input->post('bing');
		$bindo=$this->input->post('bindo');
		$ipa=$this->input->post('ipa');
		$ips=$this->input->post('ips');
		$this->m_nilai->simpan_nilai($siswa_id,$mtk,$bing,$bindo,$ipa,$ips);
		//$this->m_nilai->simpan_nilai1($siswa_id,$mtk,$bing,$bindo,$ipa,$ips);
		echo $this->session->set_flashdata('msg','<label class="label label-success">Nilai Berhasil ditambahkan</label>');
		redirect('admin/nilai');
		
		
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function tambah_nilai1(){
	if($this->session->userdata('akses')=='1'){
	$this->_set_rules();
    if ($this->form_validation->run() == true) {
		$cek = $this->db->query("SELECT * FROM tbl_nilai where siswa_id='".$this->input->post('siswa_id')."'")->num_rows();
        if ($cek<=0) {          
            $data = array(
			'siswa_id'=> $this->input->post('siswa_id'),
			'mtk'=> $this->input->post('mtk'),
			'bing'=>$this->input->post('bing'),
			'bindo'=>$this->input->post('bindo'),
			'ipa'=>$this->input->post('ipa'),
			'ips'=>$this->input->post('ips')
			);
			$this->m_nilai->simpan($data);
			echo $this->session->set_flashdata('msg','<label class="label label-success">Nilai Berhasil ditambahkan</label>');
			redirect('admin/nilai');
		 }
			echo $this->session->set_flashdata('msg','<label class="label label-danger">NIS sudah ada</label>');
			redirect('admin/nilai');
	  }else{
			redirect('admin/nilai');
		}
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function _set_rules() {
        $this->form_validation->set_rules('siswa_id', 'NIS', 'required');
    }
	function edit_nilai(){
	if($this->session->userdata('akses')=='1'){
		$kode=$this->input->post('kode');
	    $siswa_id=$this->input->post('siswa_id');
		$mtk=$this->input->post('mtk');
		$bing=$this->input->post('bing');
		$bindo=$this->input->post('bindo');
		$ipa=$this->input->post('ipa');
		$ips=$this->input->post('ips');
		$this->m_nilai->update_nilai($kode,$siswa_id,$mtk,$bing,$bindo,$ipa,$ips);
		echo $this->session->set_flashdata('msg','<label class="label label-success">nilai Berhasil diupdate</label>');
		redirect('admin/nilai');
		
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	
	function hapus_nilai(){
	if($this->session->userdata('akses')=='1'){
		$kode=$this->input->post('kode');
		$this->m_nilai->hapus_nilai($kode);
		redirect('admin/nilai');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
}