<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_laporan extends CI_Model{
	function get_data($where=''){
		return $this->db->query("
		SELECT siswa_nis, siswa_nama,
		(case siswa_jk when 1 then 'Laki-Laki' when 2 then 'Perempuan' '' end) as siswa_jk,
		siswa_kelas,siswa_thn_akd,(case siswa_status when 0 then 'Tidak Aktif' when 1 then 'Aktif' '' end) as siswa_status,
		mtk,bing,bindo,ipa,ips,
		(if(c1=1,'IPA',if(c2=1,'IPS','')))as penjurusan 
		FROM tbl_siswa INNER JOIN centroid_temp ON tbl_siswa.siswa_id=centroid_temp.siswa_id 
		LEFT JOIN tbl_nilai ON tbl_siswa.siswa_id=tbl_nilai.siswa_id
		WHERE iterasi=$where");
	}

}