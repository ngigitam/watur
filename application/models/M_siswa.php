<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_siswa extends CI_Model{
	function get_siswa(){
		$hsl=$this->db->query("SELECT siswa_id,siswa_nis,siswa_nama,(case siswa_jk when 1 then 'Laki-Laki' when 2 then 'Perempuan' '' end) as siswa_jk,siswa_kelas,siswa_thn_akd,siswa_password,siswa_level,(case siswa_status when 0 then 'Tidak Aktif' when 1 then 'Aktif' '' end) as siswa_status FROM tbl_siswa");
		return $hsl;
	}
	
	function get_siswa_ak($where=''){
		return $this->db->query("
		SELECT tbl_siswa.siswa_id,nilai_id, siswa_nis, siswa_nama,
		(case siswa_jk when 1 then 'Laki-Laki' when 2 then 'Perempuan' '' end) as siswa_jk,
		siswa_kelas,siswa_thn_akd,(case siswa_status when 0 then 'Tidak Aktif' when 1 then 'Aktif' '' end) as siswa_status,
		mtk,bing,bindo,ipa,ips,
		(if(c1=1,'IPA',if(c2=1,'IPS','')))as penjurusan 
		FROM tbl_siswa INNER JOIN centroid_temp ON tbl_siswa.siswa_id=centroid_temp.siswa_id 
		LEFT JOIN tbl_nilai ON tbl_siswa.siswa_id=tbl_nilai.siswa_id
		WHERE iterasi=$where and c1=1");
	} 
	
	function get_siswa_ap($where=''){
		return $this->db->query("
		SELECT tbl_siswa.siswa_id,nilai_id, siswa_nis, siswa_nama,
		(case siswa_jk when 1 then 'Laki-Laki' when 2 then 'Perempuan' '' end) as siswa_jk,
		siswa_kelas,siswa_thn_akd,(case siswa_status when 0 then 'Tidak Aktif' when 1 then 'Aktif' '' end) as siswa_status,
		mtk,bing,bindo,ipa,ips,
		(if(c1=1,'IPA',if(c2=1,'IPS','')))as penjurusan 
		FROM tbl_siswa INNER JOIN centroid_temp ON tbl_siswa.siswa_id=centroid_temp.siswa_id 
		LEFT JOIN tbl_nilai ON tbl_siswa.siswa_id=tbl_nilai.siswa_id
		WHERE iterasi=$where and c2=1");
	} 
	
	function simpan_siswa($nis,$nama_siswa,$jk,$kls,$thn,$password,$level){
		$hsl=$this->db->query("INSERT INTO tbl_siswa(siswa_nis,siswa_nama,siswa_jk,siswa_kelas,siswa_thn_akd,siswa_password,siswa_level,siswa_status) VALUES ('$nis','$nama_siswa','$jk','$kls','$thn',md5('$password'),'3','1')");
		return $hsl;
	}
	function simpan($data) {
        $this->db->insert('tbl_siswa', $data);
        return $this->db->insert_id();
    }

	function update_siswa_nopass($kode,$nis,$nama_siswa,$jk,$kls,$thn,$status){
		$hsl=$this->db->query("UPDATE tbl_siswa SET siswa_nis='$nis',siswa_nama='$nama_siswa',siswa_jk='$jk',siswa_kelas='$kls',siswa_thn_akd='$thn',siswa_status='$status' WHERE siswa_id='$kode'");
		return $hsl;
	}
	function update_siswa_nopass1($kode,$nis,$nama_siswa,$kls,$thn){
		$hsl=$this->db->query("UPDATE tbl_siswa SET siswa_nis='$nis',siswa_nama='$nama_siswa',siswa_kelas='$kls',siswa_thn_akd='$thn' WHERE siswa_id='$kode'");
		return $hsl;
	}
	function update_siswa($kode,$nis,$nama_siswa,$jk,$kls,$thn,$password,$status){
		$hsl=$this->db->query("UPDATE tbl_siswa SET siswa_nis='$nis',siswa_nama='$nama_siswa',siswa_jk='$jk',siswa_kelas='$kls',siswa_thn_akd='$thn',siswa_password=md5('$password'),siswa_status='$status' WHERE siswa_id='$kode'");
		return $hsl;
	}
	function update_siswa1($kode,$nis,$nama_siswa,$kls,$thn,$password){
		$hsl=$this->db->query("UPDATE tbl_siswa SET siswa_nis='$nis',siswa_nama='$nama_siswa',siswa_kelas='$kls',siswa_thn_akd='$thn',siswa_password=md5('$password') WHERE siswa_id='$kode'");
		return $hsl;
	}
	function hapus_siswa($kode){
		$hsl=$this->db->query("DELETE FROM tbl_siswa where siswa_id='$kode'");
		return $hsl;
	}
	function get_siswa1($where=''){
		$hsl=$this->db->query("SELECT siswa_id,siswa_nis,siswa_nama,
		(case siswa_jk when 1 then 'Laki-Laki' when 2 then 'Perempuan' '' end) as siswa_jk,
		siswa_kelas,siswa_thn_akd,siswa_nis,siswa_password,siswa_level,
		(case siswa_status when 0 then 'Tidak Aktif' when 1 then 'Aktif' '' end) as siswa_status 
		FROM tbl_siswa
		WHERE siswa_nis=$where");
		return $hsl;
	}
	function get_penjurusan($where='',$where1=''){
		return $this->db->query("
		SELECT siswa_nis, siswa_nama,
		(case siswa_jk when 1 then 'Laki-Laki' when 2 then 'Perempuan' '' end) as siswa_jk,
		siswa_kelas,siswa_thn_akd,(case siswa_status when 0 then 'Tidak Aktif' when 1 then 'Aktif' '' end) as siswa_status,
		mtk,bing,bindo,ipa,ips,
		(if(c1=1,'IPA',if(c2=1,'IPS','')))as penjurusan 
		FROM tbl_siswa INNER JOIN centroid_temp ON tbl_siswa.siswa_id=centroid_temp.siswa_id 
		LEFT JOIN tbl_nilai ON tbl_siswa.siswa_id=tbl_nilai.siswa_id
		WHERE iterasi=$where and siswa_nis=$where1");
	}
	
	
	function get_nilai1($where=''){
		$hsl=$this->db->query("SELECT nilai_id,tbl_nilai1.siswa_id,siswa_nis,
		siswa_nama,siswa_kelas,siswa_thn_akd,mtk,bing,bindo,ipa,ips 
		FROM tbl_nilai1 INNER JOIN tbl_siswa ON tbl_nilai1.siswa_id=tbl_siswa.siswa_id
		WHERE siswa_nis=$where");
		return $hsl;
	
	}
	
	function get_siswa2(){
        $hsl = $this->db->query("select * from tbl_siswa");
        return $hsl;  
    }
	function update_nilai1($kode,$mtk,$bing,$bindo,$ipa,$ips){
		$hsl=$this->db->query("UPDATE tbl_nilai1 SET mtk='$mtk',bing='$bing',bindo='$bindo',ipa='$ipa',ips='$ips' WHERE nilai_id='$kode'");
		return $hsl;
	}
	
	function selectdata1($where = '',$where1=''){
		return $this->db->query("select * from $where left join tbl_siswa on $where.siswa_id=tbl_siswa.siswa_id 
		WHERE siswa_nis=$where1 ;");
	}


}