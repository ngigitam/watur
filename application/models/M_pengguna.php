<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_pengguna extends CI_Model{
	function get_pengguna(){
		$hsl=$this->db->query("SELECT user_id,user_nama,user_username,user_password,(case user_level when 1 then 'Tata Usaha' when 2 then 'Kepala Sekolah' when 4 then 'Guru' when 5 then 'Wali Kelas AK' when 6 then 'Wali Kelas AP' '' end) as user_level,(case user_status when 0 then 'Tidak Aktif' when 1 then 'Aktif' '' end) as user_status  FROM user");
		return $hsl;
	}
	
	function get_pengguna1($where=''){
		$hsl=$this->db->query("SELECT user_id,user_nama,user_username,user_password,
		(case user_level when 1 then 'Tata Usaha' when 2 then 'Kepala Sekolah' when 4 then 'Guru' when 5 then 'Wali Kelas AK' when 6 then 'Wali Kelas AP' '' end) as user_level,
		(case user_status when 0 then 'Tidak Aktif' when 1 then 'Aktif' '' end) as user_status 
		FROM user
		WHERE user_id='$where'");
		return $hsl;
	}
	
	function simpan_pengguna($nama,$username,$password,$level){
		$hsl=$this->db->query("INSERT INTO user(user_nama,user_username,user_password,user_level,user_status) VALUES ('$nama','$username',md5('$password'),'$level','1')");
		return $hsl;
	}

	function update_pengguna_nopass($kode,$nama,$username,$level,$status){
		$hsl=$this->db->query("UPDATE user SET user_nama='$nama',user_username='$username',user_level='$level',user_status='$status' WHERE user_id='$kode'");
		return $hsl;
	}
	function update_pengguna_nopass1($kode,$username){
		$hsl=$this->db->query("UPDATE user SET user_username='$username' WHERE user_id='$kode'");
		return $hsl;
	}
	function update_pengguna($kode,$nama,$username,$password,$level,$status){
		$hsl=$this->db->query("UPDATE user SET user_nama='$nama',user_username='$username',user_password=md5('$password'),user_level='$level',user_status='$status' WHERE user_id='$kode'");
		return $hsl;
	}
	function update_pengguna1($kode,$username,$password){
		$hsl=$this->db->query("UPDATE user SET user_username='$username',user_password=md5('$password') WHERE user_id='$kode'");
		return $hsl;
	}
	function update_status($kode){
		$hsl=$this->db->query("UPDATE user SET user_status='0' WHERE user_id='$kode'");
		return $hsl;
	}
	function hapus_pengguna($kode){
		$hsl=$this->db->query("DELETE FROM user where user_id='$kode'");
		return $hsl;
	}
}