-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2020 at 02:27 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `watur`
--

-- --------------------------------------------------------

--
-- Table structure for table `centroid_temp`
--

CREATE TABLE `centroid_temp` (
  `id` int(5) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `iterasi` int(11) NOT NULL,
  `c1` varchar(50) NOT NULL,
  `c2` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `centroid_temp`
--

INSERT INTO `centroid_temp` (`id`, `siswa_id`, `iterasi`, `c1`, `c2`) VALUES
(1, 6, 0, '0', '0'),
(2, 6, 1, '0', '1'),
(3, 13, 0, '0', '0'),
(4, 13, 1, '1', '0'),
(5, 14, 0, '0', '0'),
(6, 14, 1, '1', '0'),
(7, 15, 0, '0', '0'),
(8, 15, 1, '0', '1'),
(9, 16, 0, '0', '0'),
(10, 16, 1, '0', '1'),
(11, 17, 0, '0', '0'),
(12, 17, 1, '0', '1'),
(13, 18, 0, '0', '0'),
(14, 18, 1, '0', '1'),
(15, 19, 0, '0', '0'),
(16, 19, 1, '1', '0'),
(17, 20, 0, '0', '0'),
(18, 20, 1, '1', '0'),
(19, 21, 0, '0', '0'),
(20, 21, 1, '0', '1'),
(21, 6, 2, '0', '1'),
(22, 13, 2, '1', '0'),
(23, 14, 2, '1', '0'),
(24, 15, 2, '0', '1'),
(25, 16, 2, '0', '1'),
(26, 17, 2, '0', '1'),
(27, 18, 2, '0', '1'),
(28, 19, 2, '1', '0'),
(29, 20, 2, '1', '0'),
(30, 21, 2, '1', '0'),
(31, 6, 3, '0', '1'),
(32, 13, 3, '1', '0'),
(33, 14, 3, '1', '0'),
(34, 15, 3, '0', '1'),
(35, 16, 3, '0', '1'),
(36, 17, 3, '0', '1'),
(37, 18, 3, '0', '1'),
(38, 19, 3, '1', '0'),
(39, 20, 3, '1', '0'),
(40, 21, 3, '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hasil_centroid`
--

CREATE TABLE `hasil_centroid` (
  `nomor` int(2) NOT NULL,
  `c1a` varchar(50) NOT NULL,
  `c1b` varchar(50) NOT NULL,
  `c1c` varchar(50) NOT NULL,
  `c1d` varchar(50) NOT NULL,
  `c1e` varchar(50) NOT NULL,
  `c2a` varchar(50) NOT NULL,
  `c2b` varchar(50) NOT NULL,
  `c2c` varchar(50) NOT NULL,
  `c2d` varchar(50) NOT NULL,
  `c2e` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil_centroid`
--

INSERT INTO `hasil_centroid` (`nomor`, `c1a`, `c1b`, `c1c`, `c1d`, `c1e`, `c2a`, `c2b`, `c2c`, `c2d`, `c2e`) VALUES
(0, '75', '76', '75', '80', '71', '83', '85', '86', '76', '84'),
(1, '79.25', '79.75', '83', '80.5', '73', '80.5', '82.666666666667', '84.666666666667', '81.5', '80.166666666667'),
(2, '78.8', '80.6', '84', '80.2', '72.8', '81.2', '82.4', '84', '82', '81.8'),
(3, '78.8', '80.6', '84', '80.2', '72.8', '81.2', '82.4', '84', '82', '81.8');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mapel`
--

CREATE TABLE `tbl_mapel` (
  `kd_mapel` varchar(10) NOT NULL,
  `nama_mapel` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mapel`
--

INSERT INTO `tbl_mapel` (`kd_mapel`, `nama_mapel`) VALUES
('MP001', 'Matematika'),
('MP002', 'Bahasa Inggris'),
('MP003', 'Bahasa Indonesia'),
('MP004', 'Pengenalan Akuntansi Perkantor'),
('MP005', 'Pengenalan Administarasi Perkantoran');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_nilai`
--

CREATE TABLE `tbl_nilai` (
  `nilai_id` int(5) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `mtk` int(10) NOT NULL,
  `bing` int(10) NOT NULL,
  `bindo` int(10) NOT NULL,
  `ipa` int(10) NOT NULL,
  `ips` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_nilai`
--

INSERT INTO `tbl_nilai` (`nilai_id`, `siswa_id`, `mtk`, `bing`, `bindo`, `ipa`, `ips`) VALUES
(3, 6, 83, 79, 83, 83, 80),
(4, 13, 81, 77, 83, 80, 72),
(5, 14, 78, 82, 81, 80, 77),
(6, 15, 84, 83, 83, 83, 77),
(7, 16, 83, 82, 80, 80, 82),
(8, 17, 78, 87, 85, 84, 86),
(9, 18, 78, 81, 89, 80, 84),
(10, 19, 79, 76, 87, 78, 71),
(11, 20, 79, 84, 81, 84, 72),
(12, 21, 77, 84, 88, 79, 72);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pengaturan`
--

CREATE TABLE `tbl_pengaturan` (
  `pengaturan_id` int(11) NOT NULL,
  `nama_jurusan` varchar(50) NOT NULL,
  `mtk` int(10) NOT NULL,
  `bing` int(10) NOT NULL,
  `bindo` int(10) NOT NULL,
  `ipa` int(10) NOT NULL,
  `ips` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pengaturan`
--

INSERT INTO `tbl_pengaturan` (`pengaturan_id`, `nama_jurusan`, `mtk`, `bing`, `bindo`, `ipa`, `ips`) VALUES
(1, 'IPA', 75, 76, 75, 80, 71),
(2, 'IPS', 83, 85, 86, 76, 84);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_siswa`
--

CREATE TABLE `tbl_siswa` (
  `siswa_id` int(11) NOT NULL,
  `siswa_nis` varchar(30) DEFAULT NULL,
  `siswa_nama` varchar(35) DEFAULT NULL,
  `siswa_jk` tinyint(1) NOT NULL,
  `siswa_kelas` varchar(5) NOT NULL,
  `siswa_thn_akd` varchar(10) NOT NULL,
  `siswa_password` varchar(35) DEFAULT NULL,
  `siswa_level` varchar(2) DEFAULT NULL,
  `siswa_status` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_siswa`
--

INSERT INTO `tbl_siswa` (`siswa_id`, `siswa_nis`, `siswa_nama`, `siswa_jk`, `siswa_kelas`, `siswa_thn_akd`, `siswa_password`, `siswa_level`, `siswa_status`) VALUES
(6, '001', 'Siswa1', 1, 'X.1', '2010/2011', 'dc5c7986daef50c1e02ab09b442ee34f', '3', '1'),
(13, '002', 'Siswa2', 1, 'X.1', '2010/2011', '93dd4de5cddba2c733c65f233097f05a', '3', '1'),
(14, '003', 'Siswa3', 1, 'X.1', '2010/2011', 'e88a49bccde359f0cabb40db83ba6080', '3', '1'),
(15, '004', 'Siswa4', 1, 'X.1', '2010/2011', '11364907cf269dd2183b64287156072a', '3', '1'),
(16, '005', 'Siswa5', 1, 'X.1', '2010/2011', 'ce08becc73195df12d99d761bfbba68d', '3', '1'),
(17, '006', 'Siswa6', 2, 'X.1', '2010/2011', '568628e0d993b1973adc718237da6e93', '3', '1'),
(18, '007', 'Siswa7', 1, 'X.1', '2010/2011', '9e94b15ed312fa42232fd87a55db0d39', '3', '1'),
(19, '008', 'Siswa8', 1, 'X.1', '2010/2011', 'a13ee062eff9d7295bfc800a11f33704', '3', '1'),
(20, '009', 'Siswa9', 2, 'X.1', '2010/2011', 'dc5e819e186f11ef3f59e6c7d6830c35', '3', '1'),
(21, '010', 'Siswa10', 2, 'X.1', '2010/2011', 'ea20a043c08f5168d4409ff4144f32e2', '3', '1'),
(37, '011', 'Siswa11', 1, 'X.1', '2010/2011', '011', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_nama` varchar(35) DEFAULT NULL,
  `user_username` varchar(30) DEFAULT NULL,
  `user_password` varchar(35) DEFAULT NULL,
  `user_level` varchar(2) DEFAULT NULL,
  `user_status` varchar(2) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_nama`, `user_username`, `user_password`, `user_level`, `user_status`) VALUES
(1, 'Admin', 'Admin', '21232f297a57a5a743894a0e4a801fc3', '1', '1'),
(2, 'Kepala Sekolah', 'kepsek', '8561863b55faf85b9ad67c52b3b851ac', '2', '1'),
(6, 'Guru', 'guru', '77e69c137812518e359196bb2f5e9bb9', '4', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `centroid_temp`
--
ALTER TABLE `centroid_temp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `centroid_fk_1` (`siswa_id`);

--
-- Indexes for table `hasil_centroid`
--
ALTER TABLE `hasil_centroid`
  ADD PRIMARY KEY (`nomor`);

--
-- Indexes for table `tbl_mapel`
--
ALTER TABLE `tbl_mapel`
  ADD PRIMARY KEY (`kd_mapel`);

--
-- Indexes for table `tbl_nilai`
--
ALTER TABLE `tbl_nilai`
  ADD PRIMARY KEY (`nilai_id`),
  ADD KEY `nilai_fk_1` (`siswa_id`);

--
-- Indexes for table `tbl_pengaturan`
--
ALTER TABLE `tbl_pengaturan`
  ADD PRIMARY KEY (`pengaturan_id`);

--
-- Indexes for table `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  ADD PRIMARY KEY (`siswa_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `centroid_temp`
--
ALTER TABLE `centroid_temp`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `tbl_nilai`
--
ALTER TABLE `tbl_nilai`
  MODIFY `nilai_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_pengaturan`
--
ALTER TABLE `tbl_pengaturan`
  MODIFY `pengaturan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
  MODIFY `siswa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_nilai`
--
ALTER TABLE `tbl_nilai`
  ADD CONSTRAINT `nilai_fk_1` FOREIGN KEY (`siswa_id`) REFERENCES `tbl_siswa` (`siswa_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
